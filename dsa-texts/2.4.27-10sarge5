Subject: New Linux kernel 2.4.27 packages fix several issues

--------------------------------------------------------------------------
Debian Security Advisory DSA XXX-1                     security@debian.org
http://www.debian.org/security/                               Dann Frazier
XXXXX 8th, 2005                         http://www.debian.org/security/faq
--------------------------------------------------------------------------

Package        : kernel-source-2.4.27
Vulnerability  : several
Problem-Type   : local/remote
Debian-specific: no
CVE ID         : CVE-2006-4093 CVE-2006-4538 CVE-2006-4997 CVE-2006-5174
                 CVE-2006-5649 CVE-2006-5871
                 

Several local and remote vulnerabilities have been discovered in the Linux
kernel that may lead to a denial of service or the execution of arbitrary
code. The Common Vulnerabilities and Exposures project identifies the
following problems:

CVE-2005-4093

    Olof Johansson reported a local DoS (Denial of Service) vulnerability
    on the PPC970 platform. Unpriveleged users can hang the system by
    executing the "attn" instruction, which was not being disabled at boot.

CVE-2006-4538

    Kirill Korotaev reported a local DoS (Denial of Service) vulnerability
    on the ia64 and sparc architectures. A user could cause the system to
    crash by executing a malformed ELF binary due to insufficient verification
    of the memory layout.

CVE-2006-4997

    ADLab Venustech Info Ltd reported a potential remote DoS (Denial of
    Service) vulnerability in the IP over ATM subsystem. A remote system
    could cause the system to crash by sending specially crafted packets
    that would trigger an attempt to free an already-freed pointer
    resulting in a system crash.

CVE-2006-5174

    Martin Schwidefsky reported a potential leak of sensitive information
    on s390 systems. The copy_from_user function did not clear the remaining
    bytes of the kernel buffer after receiving a fault on the userspace
    address, resulting in a leak of uninitialized kernel memory. A local user
    could exploit this by appending to a file from a bad address.

CVE-2006-5649

    Fabio Massimo Di Nitto reported a potential remote DoS (Denial of Service)
    vulnerability on powerpc systems.  The alignment exception only
    checked the exception table for -EFAULT, not for other errors. This can
    be exploited by a local user to cause a system crash (panic).

CVE-2006-5871

    Bill Allombert reported that various mount options are ignored by smbfs
    when UNIX extensions are enabled. This includes the uid, gid and mode
    options. Client systems would silently use the server-provided settings
    instead of honoring these options, changing the security model. This
    update includes a fix from Haroldo Gamal that forces the kernel to honor
    these mount options. Note that, since the current versions of smbmount
    always pass values for these options to the kernel, it is not currently
    possible to activate unix extensions by omitting mount options. However,
    this behavior is currently consistent with the current behavior of the
    next Debian release, 'etch'.

The following matrix explains which kernel version for which architecture
fix the problems mentioned above:

                                 Debian 3.1 (sarge)
     Source                      2.4.27-10sarge5
     Alpha architecture          2.4.27-10sarge5
     ARM architecture            2.4.27-2sarge5
     Intel IA-32 architecture    2.4.27-10sarge5
     Intel IA-64 architecture    2.4.27-10sarge5
     Motorola 680x0 architecture 2.4.27-3sarge5
     Big endian MIPS             2.4.27-10.sarge4.040815-2
     Little endian MIPS          2.4.27-10.sarge4.040815-2
     PowerPC architecture        2.4.27-10sarge5
     IBM S/390 architecture      2.4.27-2sarge5
     Sun Sparc architecture      2.4.27-9sarge5

The following matrix lists additional packages that were rebuilt for
compatibility with or to take advantage of this update:

                                 Debian 3.1 (sarge)
     fai-kernels                 1.9.1sarge5
     kernel-image-2.4.27-speakup 2.4.27-1.1sarge4
     mindi-kernel                2.4.27-2sarge4
     systemimager                3.2.3-6sarge4

We recommend that you upgrade your kernel package immediately and reboot
the machine. If you have built a custom kernel from the kernel source
package, you will need to rebuild to take advantage of these fixes.

Upgrade Instructions
--------------------

wget url
        will fetch the file for you
dpkg -i file.deb
        will install the referenced file.

If you are using the apt-get package manager, use the line for
sources.list as given below:

apt-get update
        will update the internal database
apt-get upgrade
        will install corrected packages

You may use an automated update by adding the resources from the
footer to the proper configuration.


Debian GNU/Linux 3.1 alias sarge
--------------------------------


  These files will probably be moved into the stable distribution on
  its next update.

---------------------------------------------------------------------------------
For apt-get: deb http://security.debian.org/ stable/updates main
For dpkg-ftp: ftp://security.debian.org/debian-security dists/stable/updates/main
Mailing list: debian-security-announce@lists.debian.org
Package info: `apt-cache show <pkg>' and http://packages.debian.org/<pkg>
