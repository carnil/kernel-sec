--------------------------------------------------------------------------
Debian Security Advisory DSA XXX-1                     security@debian.org
http://www.debian.org/security/                               Dann Frazier
XXXXX 8th, 2007                         http://www.debian.org/security/faq
--------------------------------------------------------------------------

Package        : linux-2.6
Vulnerability  : several
Problem-Type   : local/remote
Debian-specific: no
CVE ID         : CVE-2007-1496 CVE-2007-1497 CVE-2007-1861

Several local and remote vulnerabilities have been discovered in the Linux
kernel that may lead to a denial of service or the execution of arbitrary
code. The Common Vulnerabilities and Exposures project identifies the
following problems:

CVE-2007-1496

    Michal Miroslaw reported a DoS vulnerability (crash) in netfilter.
    A remote attacker can cause a NULL pointer dereference in the
    nfnetlink_log function.


CVE-2007-1497

    Patrick McHardy reported an vulnerability in netfilter that may
    allow attackers to bypass certain firewall rules. The nfctinfo
    value of reassembled IPv6 packet fragments were incorrectly initalized
    to 0 which allowed these packets to become tracked as ESTABLISHED.

CVE-2007-1861

    Jaco Kroon reported a bug in which NETLINK_FIB_LOOKUP packages were
    incorrectly routed back to the kernel resulting in an infinite
    recursion condition. Local users can exploit this behavior
    to cause a DoS (crash).

This problem has been fixed in the stable distribution in version 
2.6.18.dfsg.1-12etch2.

The following matrix lists additional packages that were rebuilt for
compatibility with or to take advantage of this update:

                                 Debian 4.0 (etch)
     fai-kernels                 1.17+etch2
     user-mode-linux             2.6.18-1um-2etch2

We recommend that you upgrade your kernel package immediately and reboot
the machine. If you have built a custom kernel from the kernel source
package, you will need to rebuild to take advantage of these fixes.

Upgrade Instructions
--------------------

wget url
        will fetch the file for you
dpkg -i file.deb
        will install the referenced file.

If you are using the apt-get package manager, use the line for
sources.list as given below:

apt-get update
        will update the internal database
apt-get upgrade
        will install corrected packages

You may use an automated update by adding the resources from the
footer to the proper configuration.


Debian GNU/Linux 4.0 alias etch
--------------------------------


  These files will probably be moved into the stable distribution on
  its next update.

---------------------------------------------------------------------------------
For apt-get: deb http://security.debian.org/ etch/updates main
For dpkg-ftp: ftp://security.debian.org/debian-security dists/etch/updates/main
Mailing list: debian-security-announce@lists.debian.org
Package info: `apt-cache show <pkg>' and http://packages.debian.org/<pkg>
