----------------------------------------------------------------------
Debian Security Advisory DSA-1872-1                security@debian.org
http://www.debian.org/security/                           dann frazier
August 24, 2009                     http://www.debian.org/security/faq
----------------------------------------------------------------------

Package        : linux-2.6
Vulnerability  : denial of service/privilege escalation/information leak
Problem type   : local
Debian-specific: no
CVE Id(s)      : CVE-2009-2698 CVE-2009-2846 CVE-2009-2847 CVE-2009-2848
                 CVE-2009-2849
                 
Several vulnerabilities have been discovered in the Linux kernel that
may lead to denial of service, privilege escalation or a leak of
sensitive memory. The Common Vulnerabilities and Exposures project
identifies the following problems:

CVE-2009-2698

    Herbert Xu discovered an issue in the way UDP tracks corking
    status that could allow local users to cause a denial of service
    (system crash). Tavis Ormandy and Julien Tinnes discovered that
    this issue could also be used by local users to gain elevated
    privileges.

CVE-2009-2846

    Michael Buesch noticed a typing issue in the eisa-eeprom driver
    for the hppa architecture. Local users could exploit this issue to
    gain access to restricted memory.

CVE-2009-2847

    Ulrich Drepper noticed an issue in the do_sigalstack routine on
    64-bit systems. This issue allows local users to gain access to
    potentially sensitive memory on the kernel stack.

CVE-2009-2848

    Eric Dumazet discovered an issue in the execve path, where the
    clear_child_tid variable was not being properly cleared. Local
    users could exploit this issue to cause a denial of service
    (memory corruption).

CVE-2009-2849

    Neil Brown discovered an issue in the sysfs interface to md
    devices. When md arrays are not active, local users can exploit
    this vulnerability to cause a denial of service (oops).

For the oldstable distribution (etch), this problem has been fixed in
version 2.6.18.dfsg.1-24etch4.

We recommend that you upgrade your linux-2.6, fai-kernels, and
user-mode-linux packages.

Note: Debian carefully tracks all known security issues across every
linux kernel package in all releases under active security support.
However, given the high frequency at which low-severity security
issues are discovered in the kernel and the resource requirements of
doing an update, updates for lower priority issues will normally not
be released for all kernels at the same time. Rather, they will be
released in a staggered or "leap-frog" fashion.

The following matrix lists additional source packages that were rebuilt for
compatability with or to take advantage of this update:

                                             Debian 4.0 (etch)
     fai-kernels                             1.17+etch.24etch4
     user-mode-linux                         2.6.18-1um-2etch.24etch4

Upgrade instructions
--------------------

wget url
        will fetch the file for you
dpkg -i file.deb
        will install the referenced file.

If you are using the apt-get package manager, use the line for
sources.list as given below:

apt-get update
        will update the internal database
apt-get upgrade
        will install corrected packages

You may use an automated update by adding the resources from the
footer to the proper configuration.

Debian GNU/Linux 4.0 alias etch
-------------------------------

Source archives:

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.18.dfsg.1-24etch4.diff.gz
    Size/MD5 checksum:  5562205 77430d6cfab939a4d1c82fab6ab70af3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.18.dfsg.1-24etch4.dsc
    Size/MD5 checksum:     5672 733c4de16e92e78c23341c948c2b3e37
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.18.dfsg.1.orig.tar.gz
    Size/MD5 checksum: 52225460 6a1ab0948d6b5b453ea0fce0fcc29060
  http://security.debian.org/pool/updates/main/f/fai-kernels/fai-kernels_1.17+etch.24etch4.tar.gz
    Size/MD5 checksum:    59372 8f60164e762c338a2d2079eda83c9b68
  http://security.debian.org/pool/updates/main/f/fai-kernels/fai-kernels_1.17+etch.24etch4.dsc
    Size/MD5 checksum:      740 710f999fbfec7dbbee77d348a1dd244e
  http://security.debian.org/pool/updates/main/u/user-mode-linux/user-mode-linux_2.6.18-1um-2etch.24etch4.diff.gz
    Size/MD5 checksum:    21030 6d4d20763b630aa689b0b138ded756b2
  http://security.debian.org/pool/updates/main/u/user-mode-linux/user-mode-linux_2.6.18-1um.orig.tar.gz
    Size/MD5 checksum:    14435 4d10c30313e11a24621f7218c31f3582
  http://security.debian.org/pool/updates/main/u/user-mode-linux/user-mode-linux_2.6.18-1um-2etch.24etch4.dsc
    Size/MD5 checksum:      892 e4bec3b34d424dea506a3a6ed4f815e4

Architecture independent packages:

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-manual-2.6.18_2.6.18.dfsg.1-24etch4_all.deb
    Size/MD5 checksum:  1106754 784c53a2a3feae6160564b0f0e7dc007
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-source-2.6.18_2.6.18.dfsg.1-24etch4_all.deb
    Size/MD5 checksum: 42135958 ef71049a4dc7c64a8ca3192ad9449519
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-support-2.6.18-6_2.6.18.dfsg.1-24etch4_all.deb
    Size/MD5 checksum:  3756268 c5e762c82dd9167192ebe7665b00d1d7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-doc-2.6.18_2.6.18.dfsg.1-24etch4_all.deb
    Size/MD5 checksum:  3755558 4e26460f729469e3bf131cb1a1dbeab8
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-tree-2.6.18_2.6.18.dfsg.1-24etch4_all.deb
    Size/MD5 checksum:    58560 743409764b9885dcb83c68dac363164d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-patch-debian-2.6.18_2.6.18.dfsg.1-24etch4_all.deb
    Size/MD5 checksum:  1837790 c9ae535585a5459ea473c2497d1c0ce4

alpha architecture (DEC Alpha)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-alpha-generic_2.6.18.dfsg.1-24etch4_alpha.deb
    Size/MD5 checksum: 23392288 920ff223dd59fe0eaf89a325b0f632d0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-24etch4_alpha.deb
    Size/MD5 checksum:    57878 63f539fabbf66f7678decc5d8d5413da
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-24etch4_alpha.deb
    Size/MD5 checksum:  3001340 abaf85bfe9ac6bd8768187e8c63677f9
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-alpha-legacy_2.6.18.dfsg.1-24etch4_alpha.deb
    Size/MD5 checksum: 23373060 f5670bb9aaba8a44cc4f9137564490ef
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-alpha-smp_2.6.18.dfsg.1-24etch4_alpha.deb
    Size/MD5 checksum:   266026 e8833203f6b9ea59666352fa0366b41f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-alpha-smp_2.6.18.dfsg.1-24etch4_alpha.deb
    Size/MD5 checksum: 23752700 a83eca12e501032b09780de9ebbb82da
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-alpha-legacy_2.6.18.dfsg.1-24etch4_alpha.deb
    Size/MD5 checksum:   266848 8ff085c0db4e4f5c87127bdf8265e3a4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-alpha-generic_2.6.18.dfsg.1-24etch4_alpha.deb
    Size/MD5 checksum:   266598 4c3c20b88a233f4302922efec2fbcd74
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-alpha_2.6.18.dfsg.1-24etch4_alpha.deb
    Size/MD5 checksum: 23441084 ad8f9bfd39e83af48f6c4f2792eff22d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-24etch4_alpha.deb
    Size/MD5 checksum:  2977784 34e9b0a84b0e15ff821d751405b4aaf1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-alpha_2.6.18.dfsg.1-24etch4_alpha.deb
    Size/MD5 checksum:    57912 50b2d36c2fe7566eb9f2c311e369fa2d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-alpha_2.6.18.dfsg.1-24etch4_alpha.deb
    Size/MD5 checksum:   267324 b71f2d4cd480a159da3c9d408f130374

amd64 architecture (AMD x86_64 (AMD64))

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-amd64_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum: 16953840 1f0335e59b889438fd922f4bc8e01fb4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum:  3256334 5a9700a9b7b254a848d4f366dada4f20
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-amd64_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum:    57908 2f6eabe586bc1b42e9baa712db83cebc
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum:  3232218 3d214ccd614a0e5b01947ad1e80d8b08
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-vserver_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum:  3426116 2e04116680944c93578b332680ca7bdc
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum:  3401474 d5cf8e17da27b3fa089bb1c0c9e1fbb0
  http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.18-6-xen-vserver-amd64_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum:    57866 4fc97d8c25407f09fe75b048ec098a53
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-amd64_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum: 16915152 35f30bc1fbd2e8d2f2dba1d736d9b5ff
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-vserver-amd64_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum:   281284 19bec646f72897b0f3cbf8c5b8aaf3b2
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.18-6-xen-amd64_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum: 15358464 a17684bd39f8e1b26ed9a07a67013152
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum:    57880 faa093b6cfe9e02212f3f43a4869ecfb
  http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.18-6-xen-amd64_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum:    57858 de2c1e371896a31b83f7086748c9c534
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-amd64_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum:   281636 7ef4e16987de43bae1f95e700021ce7f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-xen-amd64_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum:  1656210 e078af1e2598fb3867022ffe9360463c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-amd64_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum:   282470 d7e583361d216f95ef19e1321ad284b9
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.18-6-xen-vserver-amd64_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum: 15370258 500ad82113ee4298ca633b160a9f2b20
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-amd64_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum:   281094 4bc6bbaafa3ae15402ba876cb66daef7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-xen-vserver-amd64_2.6.18.dfsg.1-24etch4_amd64.deb
    Size/MD5 checksum:  1687830 bbcd3b3203db1dc411540c2c09db5290
  http://security.debian.org/pool/updates/main/f/fai-kernels/fai-kernels_1.17+etch.24etch4_amd64.deb
    Size/MD5 checksum:  5962194 cf2ab61a56ee4f4eb8e6f5a11b3ee078

arm architecture (ARM)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-24etch4_arm.deb
    Size/MD5 checksum:  3415016 c6f108b5e1667d1b5505e214ba348471
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-rpc_2.6.18.dfsg.1-24etch4_arm.deb
    Size/MD5 checksum:  4592974 ccb57e2f070a3a9c69801508f71b2066
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-24etch4_arm.deb
    Size/MD5 checksum:    57956 11ee055c1f79ee1057021aabbc02a05a
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-ixp4xx_2.6.18.dfsg.1-24etch4_arm.deb
    Size/MD5 checksum:   242788 fbc4c21a40cbb40b2c9ceb107fb903f5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-arm_2.6.18.dfsg.1-24etch4_arm.deb
    Size/MD5 checksum:    57998 1f6973b39040b138cb2d3245c38a1b21
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-ixp4xx_2.6.18.dfsg.1-24etch4_arm.deb
    Size/MD5 checksum:  8876162 27eaabb721027f73f2e593fce8cc25d3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-iop32x_2.6.18.dfsg.1-24etch4_arm.deb
    Size/MD5 checksum:  7929396 8a64112797bf1ea3e42a524e1d2ccfa5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-footbridge_2.6.18.dfsg.1-24etch4_arm.deb
    Size/MD5 checksum:   236722 002bde78d37e5100c337a5ac5f558f3b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-s3c2410_2.6.18.dfsg.1-24etch4_arm.deb
    Size/MD5 checksum:  5016396 042ea31fe55ac4a1f59cda4deb21619b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-footbridge_2.6.18.dfsg.1-24etch4_arm.deb
    Size/MD5 checksum:  7573354 01d3a8eb5549856579ce07b7aa51d27e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-s3c2410_2.6.18.dfsg.1-24etch4_arm.deb
    Size/MD5 checksum:   207700 f54d3f032a280b4ea81093802e84677c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-rpc_2.6.18.dfsg.1-24etch4_arm.deb
    Size/MD5 checksum:   203162 2283ab0c3b916a5d1febfeb9a3206265
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-iop32x_2.6.18.dfsg.1-24etch4_arm.deb
    Size/MD5 checksum:   237414 1521d3fb098c42193dd8164091e787df

hppa architecture (HP PA RISC)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-hppa_2.6.18.dfsg.1-24etch4_hppa.deb
    Size/MD5 checksum:    57982 498cc31776ca52785924987b8475c410
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-parisc-smp_2.6.18.dfsg.1-24etch4_hppa.deb
    Size/MD5 checksum: 11004714 da884429c9c84920673f3bde33ed81f8
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-parisc_2.6.18.dfsg.1-24etch4_hppa.deb
    Size/MD5 checksum: 10562930 cea0e57b2e92d02f82cbe8824c5bf265
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-parisc64_2.6.18.dfsg.1-24etch4_hppa.deb
    Size/MD5 checksum: 11403998 9fb3b63fcb8dfee7b70d568f827e42b6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-parisc64_2.6.18.dfsg.1-24etch4_hppa.deb
    Size/MD5 checksum:   202072 e2ac76b0f6ed13782ba148a0bfbaa993
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-parisc_2.6.18.dfsg.1-24etch4_hppa.deb
    Size/MD5 checksum:   200604 7af4ba971a1820457a31d9c6be81b04f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-parisc64-smp_2.6.18.dfsg.1-24etch4_hppa.deb
    Size/MD5 checksum:   203106 e5e653be6fe1e554cc0229175002d41e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-24etch4_hppa.deb
    Size/MD5 checksum:  3026392 f525ab46801c56f3807415333e2738f6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-24etch4_hppa.deb
    Size/MD5 checksum:    57954 3f7597851fb5b00af5a478c7a7ea4fca
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-parisc64-smp_2.6.18.dfsg.1-24etch4_hppa.deb
    Size/MD5 checksum: 11814246 46ec6cbaa14d844a4c04eed0a5ecc97e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-parisc-smp_2.6.18.dfsg.1-24etch4_hppa.deb
    Size/MD5 checksum:   201928 c4e6a5a5a5871aa8b4c1b4c56f36d93d

i386 architecture (Intel ia32)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-k7_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum: 16639318 3a3f022a960bf3a533ae4466d59593c7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-k7_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum: 16596208 b9537994d209c2c36681283929d2343f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-amd64_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:   279334 78a9a3b6e0c25efa7c6d375ec3239a75
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-486_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum: 16316006 90a4ff2365f3a8d6db271b5395996328
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-686_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:   279826 59f9787db21d5fde12370dc5b4532c72
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-486_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:   290060 c02fa416310e6eed8c31078ecda5a7c4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-vserver_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:  3238856 c74408a12a32f2e5c667463d2882aa01
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.18-6-xen-686_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum: 14376590 9ae2f1317fb7386461b01424fec7d619
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-686_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:   287268 ca067e4734079188eb4caed8bf12f697
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-xen-vserver-686_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:  1332090 353d8fad0153d2b5347a1aef1d4ea64c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:  3232878 ba734c74d4842e493909f98fac600df6
  http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.18-6-xen-686_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:    57932 ff48905f692086e6fd76871fe07cff89
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:  3216410 87bb212fe64e4433052eb08080f3ce8c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.18-6-xen-vserver-686_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum: 14391130 f165871d0c99d56f8181056aa8d6b258
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-k7_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:   286146 43a2fa642d428e033fc3b23374e10e6f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-686-bigmem_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:   287756 149d5dfe24c94c9a3ce3c9cff3cf4031
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-amd64_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum: 16928230 0c98acfc7bfc0ce53717cecb447ea71d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-xen-686_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:  1304480 0a967e79e6903892ebfd12880c2e1bcf
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-686_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:   285978 6bbf01170971c9a8f76ecf4de5bc12e5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:    57942 f06c11f8a4042303e8b679c2bb74eb9e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-686_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum: 16463874 0d283f61abfc1f2f24611a5b6434fdcb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-vserver-686_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:   280296 0851f1d12ce31f44d5b3139289fe13eb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-i386_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:    58002 c420bd6512632679d12f84eb643bd153
  http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.18-6-xen-vserver-686_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:    57950 5f038c617159ba6fee7f009814c47ea6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:  3119876 46fb19e983c4eb580c0ca4904c620dbe
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-k7_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum:   287266 c0307417ef9ec40b6c80a0b7d500ab52
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-686-bigmem_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum: 16537476 1653d19107d1cfb2449c142954f4dfda
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-686_2.6.18.dfsg.1-24etch4_i386.deb
    Size/MD5 checksum: 16501886 f97a4864403381ebcae0b1fd2a3e5506
  http://security.debian.org/pool/updates/main/f/fai-kernels/fai-kernels_1.17+etch.24etch4_i386.deb
    Size/MD5 checksum:  5510640 0504573999211a53d3f4d2751298a199
  http://security.debian.org/pool/updates/main/u/user-mode-linux/user-mode-linux_2.6.18-1um-2etch.24etch4_i386.deb
    Size/MD5 checksum: 25604240 7459aa8e287b274c92492234039cc701

ia64 architecture (Intel ia64)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-itanium_2.6.18.dfsg.1-24etch4_ia64.deb
    Size/MD5 checksum:   259432 9c2e1a0b961d6b0b659b8ff985c1eafb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-mckinley_2.6.18.dfsg.1-24etch4_ia64.deb
    Size/MD5 checksum: 28195726 dbd96cd8e523c54ba78b71725cdaaa3f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-24etch4_ia64.deb
    Size/MD5 checksum:  3086728 3eeca5f5ee087983471c8a293dfa8e69
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-24etch4_ia64.deb
    Size/MD5 checksum:    57880 e17da73dd79d04a948197966e5c65e0a
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-itanium_2.6.18.dfsg.1-24etch4_ia64.deb
    Size/MD5 checksum: 28022648 bcbe313e636424ed6144d9b40223832b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-ia64_2.6.18.dfsg.1-24etch4_ia64.deb
    Size/MD5 checksum:    57900 7faea93b2ab52a4e62678dfeecd12c14
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-mckinley_2.6.18.dfsg.1-24etch4_ia64.deb
    Size/MD5 checksum:   259348 dac2da139c602413378dfe588be45f59

mips architecture (MIPS (Big Endian))

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-mips_2.6.18.dfsg.1-24etch4_mips.deb
    Size/MD5 checksum:    57928 64d14b0f88818f2d820dc8ca44456d2b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-24etch4_mips.deb
    Size/MD5 checksum:  3354336 a64e901fd034767b3eb6dd8e2e93aa1c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-r5k-ip32_2.6.18.dfsg.1-24etch4_mips.deb
    Size/MD5 checksum:   166788 0a9dae8610e99231bd058e91c63ebe9b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sb1-bcm91250a_2.6.18.dfsg.1-24etch4_mips.deb
    Size/MD5 checksum:   186564 198280a2d4453aef80fb9bf5dbec2bf4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-r4k-ip22_2.6.18.dfsg.1-24etch4_mips.deb
    Size/MD5 checksum:  8291310 8efea4e1b641cccccb74eb26f3fa3476
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sb1a-bcm91480b_2.6.18.dfsg.1-24etch4_mips.deb
    Size/MD5 checksum: 15631156 274353be61c6892043f5927404f98363
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-qemu_2.6.18.dfsg.1-24etch4_mips.deb
    Size/MD5 checksum:  6099622 0afaca21729d56fc84e88787b3ce1f75
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-24etch4_mips.deb
    Size/MD5 checksum:    57880 6676b167a4ae486601f6d39d86fc8528
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-qemu_2.6.18.dfsg.1-24etch4_mips.deb
    Size/MD5 checksum:   154174 83817881e79dd886a0304d872ab298b3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sb1-bcm91250a_2.6.18.dfsg.1-24etch4_mips.deb
    Size/MD5 checksum: 15660556 83d11d4d8ccc1578ce602219cef24697
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sb1a-bcm91480b_2.6.18.dfsg.1-24etch4_mips.deb
    Size/MD5 checksum:   186346 4dbe7c8ff81821d8023594a08fea857d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-r5k-ip32_2.6.18.dfsg.1-24etch4_mips.deb
    Size/MD5 checksum:  9057858 271140189fe54ae8c40d266ad22c4051
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-r4k-ip22_2.6.18.dfsg.1-24etch4_mips.deb
    Size/MD5 checksum:   163208 4b8ba7697467633dc0eaab58e87813d9

mipsel architecture (MIPS (Little Endian))

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sb1a-bcm91480b_2.6.18.dfsg.1-24etch4_mipsel.deb
    Size/MD5 checksum:   186210 cf8e8c63368cb17d778ff105a32d97a6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-r5k-cobalt_2.6.18.dfsg.1-24etch4_mipsel.deb
    Size/MD5 checksum:   182242 542bfc88acb915d6a1895207e5c7053d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-24etch4_mipsel.deb
    Size/MD5 checksum:    57880 4bc6d603a82b33c6b7d5e25ae15c6d91
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-qemu_2.6.18.dfsg.1-24etch4_mipsel.deb
    Size/MD5 checksum:  6037856 9d4c290e92053430559195268eb05bf5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-r4k-kn04_2.6.18.dfsg.1-24etch4_mipsel.deb
    Size/MD5 checksum:   159482 022651f74bf8d12b21671c1e3b9107a7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-r5k-cobalt_2.6.18.dfsg.1-24etch4_mipsel.deb
    Size/MD5 checksum:  9864958 f07455b81817651a1496b324a525a6a0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-qemu_2.6.18.dfsg.1-24etch4_mipsel.deb
    Size/MD5 checksum:   154156 c9dde04a01b4244a1db56010c877d123
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-r4k-kn04_2.6.18.dfsg.1-24etch4_mipsel.deb
    Size/MD5 checksum:  5930722 c8eb0c9e24796ff37b69a7b053387be2
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-r3k-kn02_2.6.18.dfsg.1-24etch4_mipsel.deb
    Size/MD5 checksum:   159434 a2881cd4abb727c9aa4a9160248132fe
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sb1-bcm91250a_2.6.18.dfsg.1-24etch4_mipsel.deb
    Size/MD5 checksum:   186406 9e8c968109453669e2f9e34d0cc2c9fe
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sb1-bcm91250a_2.6.18.dfsg.1-24etch4_mipsel.deb
    Size/MD5 checksum: 15074416 39f984bc628733856200768ba81816a9
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-mipsel_2.6.18.dfsg.1-24etch4_mipsel.deb
    Size/MD5 checksum:    57936 9552f93c97750555e7a3f2c47a7458d0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-r3k-kn02_2.6.18.dfsg.1-24etch4_mipsel.deb
    Size/MD5 checksum:  5951606 af741dd871be26af8f0e04d1a63f4b70
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sb1a-bcm91480b_2.6.18.dfsg.1-24etch4_mipsel.deb
    Size/MD5 checksum: 15046214 8ff302c317ea6f45ee653afd44a163c7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-24etch4_mipsel.deb
    Size/MD5 checksum:  3354590 98d6808b346ef7b0d507b64a6621ab78

powerpc architecture (PowerPC)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-prep_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum: 16411244 a7935a4229c5e0d593ab76db93171d1b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-powerpc-miboot_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum: 15164116 7d3d8aa85e3a997c404e569b8e30d298
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum:  3419764 afa126b123f4abfaedaccc1110e62c09
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum:  3397416 80ef10d492ef827d62e45ed73a2c8fa6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-powerpc_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum: 16634994 bc5dbe11ef983d3fde1dd0224ebc9a5a
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-powerpc64_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum:   258548 bc0fff5d685fd70c36a70a77631e8fbd
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-powerpc_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum:    57926 d6391047097dd9549412389891da39e0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum:    57888 07fdb1f0ec29f9727805dbac661f586b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-powerpc_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum:   256932 5968ddaba2dd0ffd034aeceeb6b9c5c9
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-powerpc-smp_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum: 16975952 adf4fd623c19232d6ae3287a83572ce3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-powerpc_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum:   256004 31b75bd00c901b7a4c4247cfc8b9f428
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-powerpc64_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum: 18317366 d41b25f12d7c6d7e3817e41a4cb20baf
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-powerpc-miboot_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum:   233462 982625815c563debacd2fc5cd43d2399
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-powerpc64_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum: 18365532 4a3010ebe14a8cc6f028a656224f74a5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-powerpc_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum: 17015496 1c60889d7646d6a07c193725edb1c6d4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-prep_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum:   249654 c37777163eff0bc7f972f100c2f18c16
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-powerpc64_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum:   257254 9c153115ad54f92004bf4a6bc2a56e38
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-powerpc-smp_2.6.18.dfsg.1-24etch4_powerpc.deb
    Size/MD5 checksum:   256740 60992e008fa95164c8776604ac5267b0
  http://security.debian.org/pool/updates/main/f/fai-kernels/fai-kernels_1.17+etch.24etch4_powerpc.deb
    Size/MD5 checksum:  3371440 2265c8d13f5f4469050f6a5aadf83780

s390 architecture (IBM S/390)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-s390_2.6.18.dfsg.1-24etch4_s390.deb
    Size/MD5 checksum:    57902 bbfab01d81d99d30e56cc51af3d34604
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-24etch4_s390.deb
    Size/MD5 checksum:    57878 b0a70e581953e81489ea793f4f02b4a2
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-s390x_2.6.18.dfsg.1-24etch4_s390.deb
    Size/MD5 checksum:   149034 d5a92bb19744e05b3edc3c7f77887062
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-24etch4_s390.deb
    Size/MD5 checksum:  2947842 0fe7f591cbc050fa73992b8607365d11
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-s390x_2.6.18.dfsg.1-24etch4_s390.deb
    Size/MD5 checksum:  5670730 785d3490ebcfab4497d204111a610048
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-s390x_2.6.18.dfsg.1-24etch4_s390.deb
    Size/MD5 checksum:   148240 31655c0bd8b57737ad495ef7abca9e79
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-s390-tape_2.6.18.dfsg.1-24etch4_s390.deb
    Size/MD5 checksum:  1445382 dca95945efb37647a4207d322f133cfd
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-s390_2.6.18.dfsg.1-24etch4_s390.deb
    Size/MD5 checksum:  5409530 846b026a2393c0c8e72dc8ce91f45506
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-s390_2.6.18.dfsg.1-24etch4_s390.deb
    Size/MD5 checksum:   147846 7000581c4f6bf1fa898738f52f22513c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-24etch4_s390.deb
    Size/MD5 checksum:  2970922 b2dcb27a6525a69e7240f3d989e8b54b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-s390x_2.6.18.dfsg.1-24etch4_s390.deb
    Size/MD5 checksum:  5627252 3b31915017388cd91e87f722a77fa968

sparc architecture (Sun SPARC/UltraSPARC)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sparc32_2.6.18.dfsg.1-24etch4_sparc.deb
    Size/MD5 checksum:  6417404 a67a2b8d1a55684c1407663a992a89d0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-24etch4_sparc.deb
    Size/MD5 checksum:    57880 dd72e7a32b56da5932909690e88d6d9e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sparc32_2.6.18.dfsg.1-24etch4_sparc.deb
    Size/MD5 checksum:   168086 6dd745148f489d3ddad9cdd269d29df8
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-24etch4_sparc.deb
    Size/MD5 checksum:  3195102 4a6a505b616f32aba56bfd7749524622
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sparc64_2.6.18.dfsg.1-24etch4_sparc.deb
    Size/MD5 checksum:   199030 7bd215e7cd4429e5175fc7c9df8b28fd
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-24etch4_sparc.deb
    Size/MD5 checksum:  3173326 e847851a2bd17aaaf4471c9f33782b49
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-sparc64_2.6.18.dfsg.1-24etch4_sparc.deb
    Size/MD5 checksum: 10702930 5f14fcf7ea2b4062f81fa6451fbea522
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-sparc64_2.6.18.dfsg.1-24etch4_sparc.deb
    Size/MD5 checksum:   200982 2437f28205d68c9c88edd5a4cd73b86e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sparc64-smp_2.6.18.dfsg.1-24etch4_sparc.deb
    Size/MD5 checksum:   199994 17842a6e754d0a0242d02cb7204b41b3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sparc64_2.6.18.dfsg.1-24etch4_sparc.deb
    Size/MD5 checksum: 10392630 2cc9233d428d200807cb16558d8eb5e4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sparc64-smp_2.6.18.dfsg.1-24etch4_sparc.deb
    Size/MD5 checksum: 10657536 1390a9637d75008e87136a04f8539b31
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-sparc_2.6.18.dfsg.1-24etch4_sparc.deb
    Size/MD5 checksum:    57908 2c549f98dcf3aa02615f4eabbe3da7be

  These changes will probably be included in the oldstable distribution on
  its next update.

---------------------------------------------------------------------------------
For apt-get: deb http://security.debian.org/ oldstable/updates main
For dpkg-ftp: ftp://security.debian.org/debian-security dists/oldstable/updates/main
Mailing list: debian-security-announce@lists.debian.org
Package info: `apt-cache show <pkg>' and http://packages.debian.org/<pkg>
