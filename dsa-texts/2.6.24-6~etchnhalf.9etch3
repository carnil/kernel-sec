----------------------------------------------------------------------
Debian Security Advisory DSA-2004-1                security@debian.org
http://www.debian.org/security/                           Dann Frazier
February 27, 2010                   http://www.debian.org/security/faq
----------------------------------------------------------------------

Package        : linux-2.6.24
Vulnerability  : privilege escalation/denial of service/sensitive memory leak
Problem type   : local/remote
Debian-specific: no
CVE Id(s)      : CVE-2009-2691 CVE-2009-2695 CVE-2009-3080 CVE-2009-3726
                 CVE-2009-3889 CVE-2009-4005 CVE-2009-4020 CVE-2009-4021
                 CVE-2009-4138 CVE-2009-4308 CVE-2009-4536 CVE-2009-4538
                 CVE-2010-0003 CVE-2010-0007 CVE-2010-0291 CVE-2010-0410
                 CVE-2010-0415 CVE-2010-0622

NOTE: This kernel update marks the final planned kernel security
update for the 2.6.24 kernel in the Debian release 'etch'.  Although
security support for 'etch' officially ended on Feburary 15th, 2010,
this update was already in preparation before that date.

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a denial of service, sensitive memory leak or privilege
escalation.  The Common Vulnerabilities and Exposures project
identifies the following problems:

CVE-2009-2691

    Steve Beattie and Kees Cook reported an information leak in the
    maps and smaps files available under /proc. Local users may be
    able to read this data for setuid processes while the ELF binary
    is being loaded.

CVE-2009-2695

    Eric Paris provided several fixes to increase the protection
    provided by the mmap_min_addr tunable against NULL pointer
    dereference vulnerabilities.

CVE-2009-3080

    Dave Jones reported an issue in the gdth SCSI driver. A missing
    check for negative offsets in an ioctl call could be exploited by
    local users to create a denial of service or potentially gain
    elevated privileges.

CVE-2009-3726

    Trond Myklebust reported an issue where a malicious NFS server
    could cause a denial of service condition on its clients by
    returning incorrect attributes during an open call.

CVE-2009-3889

    Joe Malicki discovered an issue in the megaraid_sas driver.
    Insufficient permissions on the sysfs dbg_lvl interface allow
    local users to modify the debug logging behavior.

CVE-2009-4005

    Roel Kluin discovered an issue in the hfc_usb driver, an ISDN
    driver for Colognechip HFC-S USB chip. A potential read overflow
    exists which may allow remote users to cause a denial of service
    condition (oops).

CVE-2009-4020

    Amerigo Wang discovered an issue in the HFS filesystem that would
    allow a denial of service by a local user who has sufficient
    privileges to mount a specially crafted filesystem.
    
CVE-2009-4021

    Anana V. Avati discovered an issue in the fuse subsystem. If the
    system is sufficiently low on memory, a local user can cause the
    kernel to dereference an invalid pointer resulting in a denial of
    service (oops) and potentially an escalation of privileges.

CVE-2009-4138

    Jay Fenlason discovered an issue in the firewire stack that allows
    local users to cause a denial of service (oops or crash) by making
    a specially crafted ioctl call.

CVE-2009-4308

    Ted Ts'o discovered an issue in the ext4 filesystem that allows
    local users to cause a denial of service (NULL pointer
    dereference).  For this to be exploitable, the local user must
    have sufficient privileges to mount a filesystem.

CVE-2009-4536 & CVE-2009-4538

    Fabian Yamaguchi reported issues in the e1000 and e1000e drivers
    for Intel gigabit network adapters which allow remote users to
    bypass packet filters using specially crafted Ethernet frames.
    
CVE-2010-0003

    Andi Kleen reported a defect which allows local users to gain read
    access to memory reachable by the kernel when the
    print-fatal-signals option is enabled. This option is disabled by
    default.

CVE-2010-0007

    Florian Westphal reported a lack of capability checking in the
    ebtables netfilter subsystem. If the ebtables module is loaded,
    local users can add and modify ebtables rules.

CVE-2010-0291

    Al Viro reported several issues with the mmap/mremap system calls
    that allow local users to cause a denial of service (system panic)
    or obtain elevated privileges.

CVE-2010-0410

     Sebastian Krahmer discovered an issue in the netlink connector
     subsystem that permits local users to allocate large amounts of
     system memory resulting in a denial of service (out of memory).

CVE-2010-0415

    Ramon de Carvalho Valle discovered an issue in the sys_move_pages
    interface, limited to amd64, ia64 and powerpc64 flavors in Debian.
    Local users can exploit this issue to cause a denial of service
    (system crash) or gain access to sensitive kernel memory.

CVE-2010-0622

    Jermome Marchand reported an issue in the futex subsystem that
    allows a local user to force an invalid futex state which results
    in a denial of service (oops).

For the oldstable distribution (etch), this problem has been fixed in
version 2.6.24-6~etchnhalf.9etch3.

We recommend that you upgrade your linux-2.6.24 packages.

Upgrade instructions
--------------------

wget url
        will fetch the file for you
dpkg -i file.deb
        will install the referenced file.

If you are using the apt-get package manager, use the line for
sources.list as given below:

apt-get update
        will update the internal database
apt-get upgrade
        will install corrected packages

You may use an automated update by adding the resources from the
footer to the proper configuration.

Debian GNU/Linux 4.0 alias etch
-------------------------------

Oldstable updates are available for alpha, amd64, arm, hppa, i386,
ia64, mips, mipsel, powerpc, s390 and sparc.

Source archives:

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-2.6.24_2.6.24-6~etchnhalf.9etch3.dsc
    Size/MD5 checksum:     5118 e05bb21e7655cbfa39aed8d4fd6842eb
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-2.6.24_2.6.24-6~etchnhalf.9etch3.diff.gz
    Size/MD5 checksum:  4099250 127bad8d653046d37fc52115d4e3a332
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-2.6.24_2.6.24.orig.tar.gz
    Size/MD5 checksum: 59630522 6b8751d1eb8e71498ba74bbd346343af

Architecture independent packages:

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-doc-2.6.24_2.6.24-6~etchnhalf.9etch3_all.deb
    Size/MD5 checksum:  4263554 6c56ff077d17eba766af47544ce0f414
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-tree-2.6.24_2.6.24-6~etchnhalf.9etch3_all.deb
    Size/MD5 checksum:    83890 62cfd18ed176359831502e70d80b291a
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-source-2.6.24_2.6.24-6~etchnhalf.9etch3_all.deb
    Size/MD5 checksum: 46871628 328ad30d3c07f90c56d821f76e186b40
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-manual-2.6.24_2.6.24-6~etchnhalf.9etch3_all.deb
    Size/MD5 checksum:  1550090 1f114fdc3123f135017dbdcd0e4839c6
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-patch-debian-2.6.24_2.6.24-6~etchnhalf.9etch3_all.deb
    Size/MD5 checksum:  1009878 c7b7abff092940a400703b9168e46daa
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-support-2.6.24-etchnhalf.1_2.6.24-6~etchnhalf.9etch3_all.deb
    Size/MD5 checksum:    98248 a2a391008f8855d8358d5f18d9d76044

alpha architecture (DEC Alpha)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-alpha-generic_2.6.24-6~etchnhalf.9etch3_alpha.deb
    Size/MD5 checksum:   329786 a212d2b3a94f8a04611c0f20d3d324b9
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-alpha-smp_2.6.24-6~etchnhalf.9etch3_alpha.deb
    Size/MD5 checksum: 27236282 b5bc553c4bf3a49843c45814fab72443
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.9etch3_alpha.deb
    Size/MD5 checksum:    83428 f5f27b9de4905239e6315c77393f1f03
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-alpha_2.6.24-6~etchnhalf.9etch3_alpha.deb
    Size/MD5 checksum:    83454 5d152b5b6aa505982ebc7122a770b29b
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-alpha-generic_2.6.24-6~etchnhalf.9etch3_alpha.deb
    Size/MD5 checksum: 26641900 c799e7d48937975036b46edf032ecd87
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-alpha-legacy_2.6.24-6~etchnhalf.9etch3_alpha.deb
    Size/MD5 checksum: 26620162 eb1c3c27f1ac81959dc0f2ab497aee35
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.9etch3_alpha.deb
    Size/MD5 checksum:  3455268 da2d2cc2b7c4253ac408c30fcfddb28f
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-alpha-legacy_2.6.24-6~etchnhalf.9etch3_alpha.deb
    Size/MD5 checksum:   329788 f589f8815f7adf02f8884e2dd3ac613f
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-alpha-smp_2.6.24-6~etchnhalf.9etch3_alpha.deb
    Size/MD5 checksum:   329336 14bf085655b30adc8ab8f6ed4207d415

amd64 architecture (AMD x86_64 (AMD64))

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-amd64_2.6.24-6~etchnhalf.9etch3_amd64.deb
    Size/MD5 checksum: 19482308 c49d2962c1a391fb00fb1b5f0598b24e
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.9etch3_amd64.deb
    Size/MD5 checksum:  3656476 f2f5de65037664d03208fcea83bf2ee2
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.9etch3_amd64.deb
    Size/MD5 checksum:    83422 600c7216143f43f9c61b0c2ccd118ea0
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-amd64_2.6.24-6~etchnhalf.9etch3_amd64.deb
    Size/MD5 checksum:    83434 36f1d8f21ec39a473536dbeda2332e62
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-amd64_2.6.24-6~etchnhalf.9etch3_amd64.deb
    Size/MD5 checksum:   346940 d3f12fdd61f90749fdd08d857b326327

arm architecture (ARM)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-footbridge_2.6.24-6~etchnhalf.9etch3_arm.deb
    Size/MD5 checksum:  9357734 3e1165a0795d7db5f7ed8ef84205064b
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-footbridge_2.6.24-6~etchnhalf.9etch3_arm.deb
    Size/MD5 checksum:   298744 50d8bfa3c06134e190409399a36c5aa9
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.9etch3_arm.deb
    Size/MD5 checksum:    83546 1742ab93afadd1827009bf1d714e76eb
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-arm_2.6.24-6~etchnhalf.9etch3_arm.deb
    Size/MD5 checksum:    83578 07906e33f9ad267d986991c93eef1048
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-iop32x_2.6.24-6~etchnhalf.9etch3_arm.deb
    Size/MD5 checksum: 10778670 cc38a718ad5fd1c6e92d23e416610bd6
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-iop32x_2.6.24-6~etchnhalf.9etch3_arm.deb
    Size/MD5 checksum:   308138 34dbc7720b1844833f0b71aa307c37fa
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-ixp4xx_2.6.24-6~etchnhalf.9etch3_arm.deb
    Size/MD5 checksum:   310714 6a2c6fbbc1dd000b8a532227e3b8b5ae
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.9etch3_arm.deb
    Size/MD5 checksum:  3939512 91c2ba626e754fe407d6dcf3fa01337a
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-ixp4xx_2.6.24-6~etchnhalf.9etch3_arm.deb
    Size/MD5 checksum: 10786892 4d44a4ff751969855a01ad754a7c2b22

hppa architecture (HP PA RISC)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-parisc64_2.6.24-6~etchnhalf.9etch3_hppa.deb
    Size/MD5 checksum: 14375048 fe6ed4dea09aa205d801476667ef03cb
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-parisc-smp_2.6.24-6~etchnhalf.9etch3_hppa.deb
    Size/MD5 checksum: 13847788 4adc3106a987d84e12215156a379f460
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-parisc64_2.6.24-6~etchnhalf.9etch3_hppa.deb
    Size/MD5 checksum:   259624 ac09dcabb624984b7321a5f6b6dbef54
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-hppa_2.6.24-6~etchnhalf.9etch3_hppa.deb
    Size/MD5 checksum:    83578 e152e18748e5c80b6d06715db836cf83
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-parisc-smp_2.6.24-6~etchnhalf.9etch3_hppa.deb
    Size/MD5 checksum:   260838 44bc8ad5796c124b53d85a8c3a4ed912
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-parisc64-smp_2.6.24-6~etchnhalf.9etch3_hppa.deb
    Size/MD5 checksum:   262420 ff0641f04c409dd606c34373e8e16269
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-parisc64-smp_2.6.24-6~etchnhalf.9etch3_hppa.deb
    Size/MD5 checksum: 14830990 cce09e8022bee915dcde5dd8b9525428
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-parisc_2.6.24-6~etchnhalf.9etch3_hppa.deb
    Size/MD5 checksum: 13333594 a4dc863b0c84b9006c723db9a581c92e
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.9etch3_hppa.deb
    Size/MD5 checksum:    83546 990eb24056c7f6a63a4d55ec39563bae
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.9etch3_hppa.deb
    Size/MD5 checksum:  3446386 6ebfa4544252648df48cfb085cc3d2cc
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-parisc_2.6.24-6~etchnhalf.9etch3_hppa.deb
    Size/MD5 checksum:   258962 75184bed1f0b42cd8e002f93ed42198a

i386 architecture (Intel ia32)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.9etch3_i386.deb
    Size/MD5 checksum:  3656680 c5499cb98cdcdcadc48e3aa5bdf1d379
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-486_2.6.24-6~etchnhalf.9etch3_i386.deb
    Size/MD5 checksum: 19214268 e3f564cae5a85355f4b5a9248a11af98
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-686_2.6.24-6~etchnhalf.9etch3_i386.deb
    Size/MD5 checksum: 19148424 dd1d713c896888370a1667a16571c08c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-amd64_2.6.24-6~etchnhalf.9etch3_i386.deb
    Size/MD5 checksum:   346982 6a6a08f74f9690705e6d770d1f3f2566
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-486_2.6.24-6~etchnhalf.9etch3_i386.deb
    Size/MD5 checksum:   359548 b10fe011746b0df5fbd2587292af34ae
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-amd64_2.6.24-6~etchnhalf.9etch3_i386.deb
    Size/MD5 checksum: 19482314 5d9cc150e340aea40e253a757cfdc423
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-i386_2.6.24-6~etchnhalf.9etch3_i386.deb
    Size/MD5 checksum:    83452 32a1614212e964a4423b161b34cd758d
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-686-bigmem_2.6.24-6~etchnhalf.9etch3_i386.deb
    Size/MD5 checksum: 19213598 4f459c2d2cdb87a6f945cbee7d4500d4
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-686-bigmem_2.6.24-6~etchnhalf.9etch3_i386.deb
    Size/MD5 checksum:   358212 58ba32b0701643f043ab38a487cae609
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.9etch3_i386.deb
    Size/MD5 checksum:    83424 c1e8493aff96df5b0fe33f5af4686f98
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-686_2.6.24-6~etchnhalf.9etch3_i386.deb
    Size/MD5 checksum:   358752 4e3e9ef18a14fd191444591df571f80c

ia64 architecture (Intel ia64)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.9etch3_ia64.deb
    Size/MD5 checksum:  3569470 9ae824064bfc785f4b3512db78119e46
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-mckinley_2.6.24-6~etchnhalf.9etch3_ia64.deb
    Size/MD5 checksum: 32206374 badd40dd68e2c6634c65f79d9536e34d
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.9etch3_ia64.deb
    Size/MD5 checksum:    83432 64a48fa9283b1741e22f0a22dbb93b20
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-ia64_2.6.24-6~etchnhalf.9etch3_ia64.deb
    Size/MD5 checksum:    83456 235a5572d5e109a4b575080a8262dc57
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-mckinley_2.6.24-6~etchnhalf.9etch3_ia64.deb
    Size/MD5 checksum:   319938 d7dc0120458e93119879dcdd1e48017e
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-itanium_2.6.24-6~etchnhalf.9etch3_ia64.deb
    Size/MD5 checksum: 32025762 7595d7dc21d3273f46b35b8c00b0e195
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-itanium_2.6.24-6~etchnhalf.9etch3_ia64.deb
    Size/MD5 checksum:   320226 34731a37b519d726b133093e04d937c3

mips architecture (MIPS (Big Endian))

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-4kc-malta_2.6.24-6~etchnhalf.9etch3_mips.deb
    Size/MD5 checksum: 22243472 532341ea0847ea19414413f7659ff13d
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sb1a-bcm91480b_2.6.24-6~etchnhalf.9etch3_mips.deb
    Size/MD5 checksum:   248638 ce9da5c377d6328e9bb9be1c3945fff8
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-r5k-ip32_2.6.24-6~etchnhalf.9etch3_mips.deb
    Size/MD5 checksum: 12001172 817c44fd5afbeef1b9f172522ff21bcb
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-r4k-ip22_2.6.24-6~etchnhalf.9etch3_mips.deb
    Size/MD5 checksum: 10553972 20ddd95631b93efd52ae0aa38a5cd6d4
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-mips_2.6.24-6~etchnhalf.9etch3_mips.deb
    Size/MD5 checksum:    83600 a7b66d71779dea207a3d49cb9f692fdb
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-5kc-malta_2.6.24-6~etchnhalf.9etch3_mips.deb
    Size/MD5 checksum: 27858364 54998117445c20f413331d1197355745
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sb1-bcm91250a_2.6.24-6~etchnhalf.9etch3_mips.deb
    Size/MD5 checksum: 17212542 bf6c996fd387eef151e0db60d1bd00f2
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-5kc-malta_2.6.24-6~etchnhalf.9etch3_mips.deb
    Size/MD5 checksum:   313302 314b57dc807eb91f617c10b1497e1617
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-4kc-malta_2.6.24-6~etchnhalf.9etch3_mips.deb
    Size/MD5 checksum:   314602 2c127076bf189be2836a4c3a4c7736af
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.9etch3_mips.deb
    Size/MD5 checksum:  3804368 fbcb3bdd668db166ad3f08e6dbfbc6e0
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sb1a-bcm91480b_2.6.24-6~etchnhalf.9etch3_mips.deb
    Size/MD5 checksum: 17194888 9bce41a8b9936a16a3aa9cca675b9638
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.9etch3_mips.deb
    Size/MD5 checksum:    83540 8acdb1b4a4bc57f55b9cc5b2b04043a4
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-r5k-ip32_2.6.24-6~etchnhalf.9etch3_mips.deb
    Size/MD5 checksum:   229412 0b93c7c909eca04fad4fa45e3e73e96c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sb1-bcm91250a_2.6.24-6~etchnhalf.9etch3_mips.deb
    Size/MD5 checksum:   248700 13266a2acd5fcbd75d11049dd5e5ad58
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-r4k-ip22_2.6.24-6~etchnhalf.9etch3_mips.deb
    Size/MD5 checksum:   218314 4174dec1c73ca114469cbb88fba32926

mipsel architecture (MIPS (Little Endian))

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sb1-bcm91250a_2.6.24-6~etchnhalf.9etch3_mipsel.deb
    Size/MD5 checksum: 16567710 29d2ab68b4259a1822a2ca19e9494f5d
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-5kc-malta_2.6.24-6~etchnhalf.9etch3_mipsel.deb
    Size/MD5 checksum:   309868 d69b27ef946f2ac62b115e0200fe8002
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-5kc-malta_2.6.24-6~etchnhalf.9etch3_mipsel.deb
    Size/MD5 checksum: 26988356 f0d885b353b15dc42e4e76da8a8fb129
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sb1-bcm91250a_2.6.24-6~etchnhalf.9etch3_mipsel.deb
    Size/MD5 checksum:   248150 7c585f74e0752a631050b13b9740c0c3
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-r5k-cobalt_2.6.24-6~etchnhalf.9etch3_mipsel.deb
    Size/MD5 checksum: 13318088 0b0a8b724245ac10817b03c4cf734827
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-mipsel_2.6.24-6~etchnhalf.9etch3_mipsel.deb
    Size/MD5 checksum:    83484 a678208f18017a9c87d45548916fd98e
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-4kc-malta_2.6.24-6~etchnhalf.9etch3_mipsel.deb
    Size/MD5 checksum:   311392 5b1f0957a2756b04be6c95ae8ca5e2c8
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-4kc-malta_2.6.24-6~etchnhalf.9etch3_mipsel.deb
    Size/MD5 checksum: 21736368 02da1a4e543b8c5082476b156281cb31
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-r5k-cobalt_2.6.24-6~etchnhalf.9etch3_mipsel.deb
    Size/MD5 checksum:   247968 929ca712a0aa0984f9dc2a6f68f405a5
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sb1a-bcm91480b_2.6.24-6~etchnhalf.9etch3_mipsel.deb
    Size/MD5 checksum: 16632240 c9de1dfccb8a5cb5d5d652ca694a7108
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.9etch3_mipsel.deb
    Size/MD5 checksum:  3805532 be10a8b64da3adf7ece3846b0b0bf930
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.9etch3_mipsel.deb
    Size/MD5 checksum:    83434 ac0cb9b5939e4ea82c3c83a1a1d473ed
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sb1a-bcm91480b_2.6.24-6~etchnhalf.9etch3_mipsel.deb
    Size/MD5 checksum:   248174 50e84058a7d710f013f92e1fe68a705c

powerpc architecture (PowerPC)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-powerpc-smp_2.6.24-6~etchnhalf.9etch3_powerpc.deb
    Size/MD5 checksum:   322474 2d7e39cf0b78d98125a0baba377f1af0
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-powerpc_2.6.24-6~etchnhalf.9etch3_powerpc.deb
    Size/MD5 checksum: 19195556 bb2bd8e203cee7b3c6739d5c5d11901a
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-powerpc64_2.6.24-6~etchnhalf.9etch3_powerpc.deb
    Size/MD5 checksum:   324008 3b021bb4b3dac72dc68e701f4a209939
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-powerpc-miboot_2.6.24-6~etchnhalf.9etch3_powerpc.deb
    Size/MD5 checksum:   295928 ca2bf1c3c12f409e469c516877a8e91c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-powerpc64_2.6.24-6~etchnhalf.9etch3_powerpc.deb
    Size/MD5 checksum: 21170062 4022dbff73ebfde3a846ce38896cf09c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-powerpc_2.6.24-6~etchnhalf.9etch3_powerpc.deb
    Size/MD5 checksum:   322502 4b76cce255e1fcc72cb82053cd34a1a2
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-powerpc-miboot_2.6.24-6~etchnhalf.9etch3_powerpc.deb
    Size/MD5 checksum: 17459240 ad749c6e735e58d775b7190ff3d26e50
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-powerpc_2.6.24-6~etchnhalf.9etch3_powerpc.deb
    Size/MD5 checksum:    83466 a041c0fdb383832cf725723ce22e40c0
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.9etch3_powerpc.deb
    Size/MD5 checksum:    83430 392d415932625b1a69dc6494d2f737e0
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.9etch3_powerpc.deb
    Size/MD5 checksum:  3674486 200fdcca2140a97f961a37d70db620d5
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-powerpc-smp_2.6.24-6~etchnhalf.9etch3_powerpc.deb
    Size/MD5 checksum: 19487244 b42ad8431643d89a1f8b0e6e0aaeb39e

s390 architecture (IBM S/390)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.9etch3_s390.deb
    Size/MD5 checksum:    83532 a1c34683fe304f1a86bbc28f6cbc654c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-s390_2.6.24-6~etchnhalf.9etch3_s390.deb
    Size/MD5 checksum:    83556 bf7fed1ef4da92d782409fe8345f861a
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-s390_2.6.24-6~etchnhalf.9etch3_s390.deb
    Size/MD5 checksum:  6976486 5b5db16fea4336068bbcd5bff56ad575
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-s390x_2.6.24-6~etchnhalf.9etch3_s390.deb
    Size/MD5 checksum:  7228452 75c044fa17d6071de36579a1491c2e1b
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.9etch3_s390.deb
    Size/MD5 checksum:  3431908 18825f85900faca81b21e48d43af6ee7
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-s390x_2.6.24-6~etchnhalf.9etch3_s390.deb
    Size/MD5 checksum:   197006 0a44248e77ec1ff027edd032ebe5b2c6
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-s390-tape_2.6.24-6~etchnhalf.9etch3_s390.deb
    Size/MD5 checksum:  1503494 bd7f7b7bd4e120472bf60ad0b7d9184e
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-s390_2.6.24-6~etchnhalf.9etch3_s390.deb
    Size/MD5 checksum:   196810 f03114c2f256a97b15f88d2659f9501b

sparc architecture (Sun SPARC/UltraSPARC)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.9etch3_sparc.deb
    Size/MD5 checksum:    83428 1ea7179752fbb45e10e731991583db68
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sparc64_2.6.24-6~etchnhalf.9etch3_sparc.deb
    Size/MD5 checksum:   263546 ef894d6917cbe692ec9197048538d5e7
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.9etch3_sparc.deb
    Size/MD5 checksum:  3651402 a0194c650712040f81e97d5b3b62bc79
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sparc64-smp_2.6.24-6~etchnhalf.9etch3_sparc.deb
    Size/MD5 checksum:   264892 0b642e20f00b52c20b6ae9e0ee1f78b8
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-sparc_2.6.24-6~etchnhalf.9etch3_sparc.deb
    Size/MD5 checksum:    83442 6d109d7f131dab564736e2ac6a85dd29
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sparc64-smp_2.6.24-6~etchnhalf.9etch3_sparc.deb
    Size/MD5 checksum: 13318532 dbce062bfa560c331b75bed073868e1d
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sparc64_2.6.24-6~etchnhalf.9etch3_sparc.deb
    Size/MD5 checksum: 13019464 b0b153fafa43b650e996a9d84bbb26d7

  These changes will probably be included in the oldstable distribution on
  its next update.

---------------------------------------------------------------------------------
For apt-get: deb http://security.debian.org/ stable/updates main
For dpkg-ftp: ftp://security.debian.org/debian-security dists/stable/updates/main
Mailing list: debian-security-announce@lists.debian.org
Package info: `apt-cache show <pkg>' and http://packages.debian.org/<pkg>
