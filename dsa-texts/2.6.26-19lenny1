----------------------------------------------------------------------
Debian Security Advisory DSA-1915-1                security@debian.org
http://www.debian.org/security/                           dann frazier
October 22, 2009                    http://www.debian.org/security/faq
----------------------------------------------------------------------

Package        : linux-2.6
Vulnerability  : privilege escalation/denial of service/sensitive memory leak
Problem type   : local/remote
Debian-specific: no
CVE Id(s)      : CVE-2009-2695 CVE-2009-2903 CVE-2009-2908 CVE-2009-2909
                 CVE-2009-2910 CVE-2009-3001 CVE-2009-3002 CVE-2009-3286
                 CVE-2009-3290 CVE-2009-3613

Notice: Debian 5.0.4, the next point release of Debian 'lenny',
will include a new default value for the mmap_min_addr tunable.
This change will add an additional safeguard against a class of security
vulnerabilities known as "NULL pointer dereference" vulnerabilities, but
it will need to be overridden when using certain applications.
Additional information about this change, including instructions for
making this change locally in advance of 5.0.4 (recommended), can be
found at:
  http://wiki.debian.org/mmap_min_addr

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a denial of service, sensitive memory leak or privilege escalation.
The Common Vulnerabilities and Exposures project identifies the following
problems:

CVE-2009-2695

    Eric Paris provided several fixes to increase the protection
    provided by the mmap_min_addr tunable against NULL pointer
    dereference vulnerabilities.

CVE-2009-2903

    Mark Smith discovered a memory leak in the appletalk
    implementation.  When the appletalk and ipddp modules are loaded,
    but no ipddp"N" device is found, remote attackers can cause a
    denial of service by consuming large amounts of system memory.

CVE-2009-2908

    Loic Minier discovered an issue in the eCryptfs filesystem. A
    local user can cause a denial of service (kernel oops) by causing
    a dentry value to go negative.

CVE-2009-2909

    Arjan van de Ven discovered an issue in the AX.25 protocol
    implementation. A specially crafted call to setsockopt() can
    result in a denial of service (kernel oops).

CVE-2009-2910

    Jan Beulich discovered the existence of a sensitive kernel memory
    leak. Systems running the 'amd64' kernel do not properly sanitize
    registers for 32-bit processes.

CVE-2009-3001

    Jiri Slaby fixed a sensitive memory leak issue in the ANSI/IEEE
    802.2 LLC implementation. This is not exploitable in the Debian
    lenny kernel as root privileges are required to exploit this
    issue.

CVE-2009-3002

    Eric Dumazet fixed several sensitive memory leaks in the IrDA,
    X.25 PLP (Rose), NET/ROM, Acorn Econet/AUN, and Controller Area
    Network (CAN) implementations. Local users can exploit these
    issues to gain access to kernel memory.

CVE-2009-3286

    Eric Paris discovered an issue with the NFSv4 server
    implementation.  When an O_EXCL create fails, files may be left
    with corrupted permissions, possibly granting unintentional
    privileges to other local users.

CVE-2009-3290

    Jan Kiszka noticed that the kvm_emulate_hypercall function in KVM
    does not prevent access to MMU hypercalls from ring 0, which
    allows local guest OS users to cause a denial of service (guest
    kernel crash) and read or write guest kernel memory.

CVE-2009-3613

    Alistair Strachan reported an issue in the r8169 driver. Remote
    users can cause a denial of service (IOMMU space exhaustion and
    system crash) by transmitting a large amount of jumbo frames.

For the stable distribution (lenny), this problem has been fixed in
version 2.6.26-19lenny1.

For the oldstable distribution (etch), these problems, where
applicable, will be fixed in updates to linux-2.6 and linux-2.6.24.

We recommend that you upgrade your linux-2.6 and user-mode-linux
packages.

Note: Debian carefully tracks all known security issues across every
linux kernel package in all releases under active security support.
However, given the high frequency at which low-severity security
issues are discovered in the kernel and the resource requirements of
doing an update, updates for lower priority issues will normally not
be released for all kernels at the same time. Rather, they will be
released in a staggered or "leap-frog" fashion.

The following matrix lists additional source packages that were rebuilt for
compatibility with or to take advantage of this update:

                                             Debian 5.0 (lenny)
     user-mode-linux                         2.6.26-1um-2+19lenny1

Upgrade instructions
--------------------

wget url
        will fetch the file for you
dpkg -i file.deb
        will install the referenced file.

If you are using the apt-get package manager, use the line for
sources.list as given below:

apt-get update
        will update the internal database
apt-get upgrade
        will install corrected packages

You may use an automated update by adding the resources from the
footer to the proper configuration.

Debian GNU/Linux 5.0 alias lenny
--------------------------------

Stable updates are available for alpha, amd64, arm, armel, hppa, i386, ia64, mips, mipsel, powerpc, s390 and sparc.

Source archives:

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.26-19lenny1.diff.gz
    Size/MD5 checksum:  7643838 b6b5d896bbc02eea1516acefb752b028
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.26.orig.tar.gz
    Size/MD5 checksum: 61818969 85e039c2588d5bf3cb781d1c9218bbcb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.26-19lenny1.dsc
    Size/MD5 checksum:     5778 87d44ca47bc435ab72f03620a8cbcc6a

Architecture independent packages:

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-source-2.6.26_2.6.26-19lenny1_all.deb
    Size/MD5 checksum: 48675122 160e198488576fc5207a0f4f16454051
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-manual-2.6.26_2.6.26-19lenny1_all.deb
    Size/MD5 checksum:  1767094 4157e016e1c4de75246ec5b13a0b10ef
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-patch-debian-2.6.26_2.6.26-19lenny1_all.deb
    Size/MD5 checksum:  2555882 942a0fcf654d6c2aaa8ba343a87752f8
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-support-2.6.26-2_2.6.26-19lenny1_all.deb
    Size/MD5 checksum:   122028 28a7345af55f74689d73190015b00dbc
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-doc-2.6.26_2.6.26-19lenny1_all.deb
    Size/MD5 checksum:  4627708 59a9cbb3478d6f0d0ad5e7e0704e1bb8
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-tree-2.6.26_2.6.26-19lenny1_all.deb
    Size/MD5 checksum:   106722 37017679870c0c9b48eebe9a1dcc42a3

alpha architecture (DEC Alpha)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-alpha-smp_2.6.26-19lenny1_alpha.deb
    Size/MD5 checksum:   365202 81902d247bda62ede01b8e611bd8e112
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-alpha-legacy_2.6.26-19lenny1_alpha.deb
    Size/MD5 checksum:   364258 821619686e248614508b333c79b3ae9c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-alpha-smp_2.6.26-19lenny1_alpha.deb
    Size/MD5 checksum: 29177010 f1695dc7a5d9da563226e7e79f948757
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-alpha-generic_2.6.26-19lenny1_alpha.deb
    Size/MD5 checksum: 28487206 9764065c78953a81e53e9e55c13d7197
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-alpha-generic_2.6.26-19lenny1_alpha.deb
    Size/MD5 checksum:   363878 ce28435c1b749d412394f4ba9c37ae7e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-alpha-legacy_2.6.26-19lenny1_alpha.deb
    Size/MD5 checksum: 28471056 fe46b2cf35234e04792eef4e8b5ecba3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny1_alpha.deb
    Size/MD5 checksum:   106226 8f323cda74dc42f3619127fc999b8c79
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-alpha_2.6.26-19lenny1_alpha.deb
    Size/MD5 checksum:   106244 40e9431f0224f8f293babbc08201b266
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny1_alpha.deb
    Size/MD5 checksum:  3543660 84db694b4039ea42cbd59d685420cd08
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny1_alpha.deb
    Size/MD5 checksum:   741068 c3ac6c44e6dc90eb97a598a364e58417

amd64 architecture (AMD x86_64 (AMD64))

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-amd64_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum: 20885120 adddf8c6a8bc505ce04adbab3ae445ad
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-amd64_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum:   389638 3dded36588d23c3f813bc002a08b251b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-amd64_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum:   106250 bf416250c7b93a4c101cc361d1d276ce
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-amd64_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum: 20900916 55607c0b0e1c91ab8977f19dbf428621
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-xen-amd64_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum:  1804620 9d1925ed82b0fd6efa7656cfd9616c2f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum:  3751628 a35f2c71dc3a83c02a801d60538d9456
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum:   749444 63bee4147b5785272c3db232b58f1041
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-openvz_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum:  3774616 5c9c9f7e6ea78808a671863b98bb6f3b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.26-2-xen-amd64_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum: 19274338 c777c63ef88330789295746976ea88b5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-openvz-amd64_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum: 21053458 43ce00c33d321cdc37e3b752cad2135e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-amd64_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum:   387228 18fdefca4f553fe4af15a9986a8558a7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum:   106218 c287f196bc2ee66f483befcbbfa9e56c
  http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.26-2-xen-amd64_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum:   106202 b9f37a797a8dcd1ca3f3e4c50a1bff30
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-openvz-amd64_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum:   394126 08cb6584482c74cf69cbf74a7a35330d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-xen_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum:  3851376 635e1133f9136a67a33d4802096aa384
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-xen-amd64_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum:   383088 fc282fffa1021514e7337d360ca4b091
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny1_amd64.deb
    Size/MD5 checksum:  3719026 c7ee8698dad587f0dc32fab1af61d6d9

arm architecture (ARM)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-ixp4xx_2.6.26-19lenny1_arm.deb
    Size/MD5 checksum: 11718230 d143acbbce645433c28112d94f716a8c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-arm_2.6.26-19lenny1_arm.deb
    Size/MD5 checksum:   106232 1b5c94c3cad061c304acdc4b093737a3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-footbridge_2.6.26-19lenny1_arm.deb
    Size/MD5 checksum:   348688 3aee9049eb40c1c23e699240e0bb40c1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-ixp4xx_2.6.26-19lenny1_arm.deb
    Size/MD5 checksum:   364544 d3c6b2f3f5e1b9ff73a118fdc65c246e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny1_arm.deb
    Size/MD5 checksum:   106192 98864b78224331ba35b26f80a4cfbb81
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-footbridge_2.6.26-19lenny1_arm.deb
    Size/MD5 checksum: 10239548 1346c5b1ac226022cf4cadc51123041f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-iop32x_2.6.26-19lenny1_arm.deb
    Size/MD5 checksum:   366580 c3e3862542a31f0fdba610543a9162d3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-orion5x_2.6.26-19lenny1_arm.deb
    Size/MD5 checksum: 11411874 c3787349a5fb0dbae157c4d8ec80985a
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny1_arm.deb
    Size/MD5 checksum:   746752 5bbea7fb7c579d96cc2adfd04bca44ef
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny1_arm.deb
    Size/MD5 checksum:  4139798 35f72cebaab83eef03b69e6834b0fb7f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-iop32x_2.6.26-19lenny1_arm.deb
    Size/MD5 checksum: 12440096 4f0053f165052b407595a04d79e73e61
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-orion5x_2.6.26-19lenny1_arm.deb
    Size/MD5 checksum:   361536 29b7f9cbcfdf5760062c39d4b28b339d

armel architecture (ARM EABI)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny1_armel.deb
    Size/MD5 checksum:  4136688 02d5b1ffedd621cfffb0d80af4cbd57b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-ixp4xx_2.6.26-19lenny1_armel.deb
    Size/MD5 checksum: 11680306 6c951dd5782cd23939ed3cbdd04fc878
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-iop32x_2.6.26-19lenny1_armel.deb
    Size/MD5 checksum: 12396020 fc1aa7847d9bae51bf59923ddc0ad1f8
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-versatile_2.6.26-19lenny1_armel.deb
    Size/MD5 checksum:  9574892 ce86554f814f2b42a55bc6ea2e6a7106
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-armel_2.6.26-19lenny1_armel.deb
    Size/MD5 checksum:   106234 091b7751443c9277ea02883093747703
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-versatile_2.6.26-19lenny1_armel.deb
    Size/MD5 checksum:   334564 484e3df9ee4a450bbe5163926ac8e3fc
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-orion5x_2.6.26-19lenny1_armel.deb
    Size/MD5 checksum: 11371036 7a9a4f1e9f0d8995294053fe0ceb7ccd
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-iop32x_2.6.26-19lenny1_armel.deb
    Size/MD5 checksum:   366124 338117a039481e2a13fc26c0aeddd790
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny1_armel.deb
    Size/MD5 checksum:   747644 a55986dca639bc80d008d23f213284bb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-ixp4xx_2.6.26-19lenny1_armel.deb
    Size/MD5 checksum:   362964 3e0a6b4a298c6fefdc50de8092bbde04
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-orion5x_2.6.26-19lenny1_armel.deb
    Size/MD5 checksum:   359664 9d5fee325f322725775b5487741be647
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny1_armel.deb
    Size/MD5 checksum:   106194 68b88c1687cfbc3410ef247a1766da2c

hppa architecture (HP PA RISC)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-parisc-smp_2.6.26-19lenny1_hppa.deb
    Size/MD5 checksum:   296914 d74f0573e197572b6ae4824bed753ba9
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny1_hppa.deb
    Size/MD5 checksum:   758510 92a123eca1e1ab4b55908eac04a49d3e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny1_hppa.deb
    Size/MD5 checksum:   106192 7a13af4e96ed892dbcd14c31c6e3fec9
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-parisc64-smp_2.6.26-19lenny1_hppa.deb
    Size/MD5 checksum: 17600786 0da91a9e353e46954656849d90f80404
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-parisc64_2.6.26-19lenny1_hppa.deb
    Size/MD5 checksum: 17055794 bf351e3cfd31c5d340820faecd657957
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-parisc-smp_2.6.26-19lenny1_hppa.deb
    Size/MD5 checksum: 16317844 3863891a64ea935b49132deaf7d28b82
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny1_hppa.deb
    Size/MD5 checksum:  3600074 9549ee7c67d7cb01e18ebd4bd45217fb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-parisc_2.6.26-19lenny1_hppa.deb
    Size/MD5 checksum: 15724906 a6842f75a3e0363b5813c98ba8fdd93b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-hppa_2.6.26-19lenny1_hppa.deb
    Size/MD5 checksum:   106222 1690af7c5e0b89a5ccc7ba58668e9efa
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-parisc64_2.6.26-19lenny1_hppa.deb
    Size/MD5 checksum:   296178 3272eeb9db69337c3fe24214858ad133
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-parisc_2.6.26-19lenny1_hppa.deb
    Size/MD5 checksum:   295104 337f765c6bc792a3c34697ee0ef6cead
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-parisc64-smp_2.6.26-19lenny1_hppa.deb
    Size/MD5 checksum:   298994 4d57561b3d6eecbc4416b903d041f1e1

i386 architecture (Intel ia32)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-686_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum: 20235758 965766bb7bca54211d0bcc4461698822
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-686_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:   397928 ac8105d3d31eb659802e0e3f4dbb3dd1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-486_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum: 20174784 8744b68d2f257fdbd8b78c54437ce902
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-686-bigmem_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum: 20326334 7748ca82861e1bb924c0536488efeb51
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-xen-686_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:   385312 c53fba14c80d813d2510060241b17401
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-openvz-686_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum: 20501792 719ab20afef9cd103b234c74d203384d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-486_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:   398036 8ccc26cafb37c3ed3ccd9e45c58a7bcd
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-xen-686_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:  1592000 66c26539c73138df200265ea67e9127e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-686_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum: 20207632 2a43dc386ea5a94baf178b170a0a72a5
  http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.26-2-xen-686_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:   106212 fbf271a7acfe162ebcb78c88e68ed8e5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-i386_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:   106262 46194207eeebbddeba75b4c093600638
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:  3719038 0f84f28217631d02e48dd8df52056be7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-xen_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:  3851496 a9596a304602f4448a9b5c529a7ff9a1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.26-2-xen-686_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum: 18035674 6525a6e6d2328f0393a45b3f9048c44c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-686-bigmem_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum: 20353346 d51dbb2de178ba2e1589794e7fe850b7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-amd64_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum: 20864554 4291dfb20626cc00561a15292734cd4f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-amd64_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:   387194 63e644bc512a0265e07a4c0183b40532
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-686_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:   399186 2bce11e03c0f3718624911f6d9025fe5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-686-bigmem_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:   399024 38088f6a52c0d8e00429fcdd8e51f5e2
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:  3751794 7dab7fa65d5d48fa21ae2d5b680b4feb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:   106212 9e51786fdf948efe3df0c9ebf91505f4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-openvz-686_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:   403610 cdebf3cd645851c8e1c6343d13bc20ae
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:   749454 d4dbde4b155673ac110c7e9fd37123d6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-686-bigmem_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:   398430 bc279d033cf43918d0bd446feeeedffa
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-openvz_2.6.26-19lenny1_i386.deb
    Size/MD5 checksum:  3774804 fffaa64df94bc43269519da987510a06

ia64 architecture (Intel ia64)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny1_ia64.deb
    Size/MD5 checksum:   106222 ef8fef5f51e7a356be26920c3de433fb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny1_ia64.deb
    Size/MD5 checksum:   748116 72bc17a29b0bc8c2d33ef49721bf793d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny1_ia64.deb
    Size/MD5 checksum:  3654682 45c6a140c6eb393b90cc879aa6305047
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-itanium_2.6.26-19lenny1_ia64.deb
    Size/MD5 checksum: 34165154 a735a8c9b69ee109d5dac565a777464f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-itanium_2.6.26-19lenny1_ia64.deb
    Size/MD5 checksum:   354954 d1f92b5082550676d0532e0a65143f46
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-itanium_2.6.26-19lenny1_ia64.deb
    Size/MD5 checksum: 34103002 2458d02fc3943a0b60bee03455e4134f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-mckinley_2.6.26-19lenny1_ia64.deb
    Size/MD5 checksum: 34290130 657b00cb9120283655bc349e242c8790
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-19lenny1_ia64.deb
    Size/MD5 checksum:  3687274 7b3202292c6f8da793f13c08899f4eb4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-ia64_2.6.26-19lenny1_ia64.deb
    Size/MD5 checksum:   106254 fe4b7d22cbbeade4d9b38c21bf51025e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-mckinley_2.6.26-19lenny1_ia64.deb
    Size/MD5 checksum: 34348684 a83ec40a2adca957575954e2747a52f9
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-mckinley_2.6.26-19lenny1_ia64.deb
    Size/MD5 checksum:   354840 248e53bf67eefc4299bf36005fb38e91
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-itanium_2.6.26-19lenny1_ia64.deb
    Size/MD5 checksum:   355486 e5c82246e58861912eeb1389248a9582
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-mckinley_2.6.26-19lenny1_ia64.deb
    Size/MD5 checksum:   355536 8aac568adfb655a87b2ef00e3bdb9e56

mips architecture (MIPS (Big Endian))

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-r5k-ip32_2.6.26-19lenny1_mips.deb
    Size/MD5 checksum: 15646714 b7225d6485168324e780cea3d6f2d59d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-mips_2.6.26-19lenny1_mips.deb
    Size/MD5 checksum:   106278 67c2358f394f32ff2fb0af1030c52533
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-sb1a-bcm91480b_2.6.26-19lenny1_mips.deb
    Size/MD5 checksum: 20122360 febab78c12a6a3268f833087c0c56486
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny1_mips.deb
    Size/MD5 checksum:   742200 70097bfb5c777d3920d570b52ae432ec
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-r5k-ip32_2.6.26-19lenny1_mips.deb
    Size/MD5 checksum:   277818 95a0c1559d24890aea2e3b01f84548e1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-4kc-malta_2.6.26-19lenny1_mips.deb
    Size/MD5 checksum: 23353804 bb8a2410d92de6e6ee4b095d0079a304
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-sb1-bcm91250a_2.6.26-19lenny1_mips.deb
    Size/MD5 checksum: 20134016 bd1b6e42923dc7ecbf1db589f2e050dc
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-sb1-bcm91250a_2.6.26-19lenny1_mips.deb
    Size/MD5 checksum:   293936 b038878251981b0f5a14c75f8d500e72
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-5kc-malta_2.6.26-19lenny1_mips.deb
    Size/MD5 checksum: 29270186 075bc506b69238577b3075ecce90cea8
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-5kc-malta_2.6.26-19lenny1_mips.deb
    Size/MD5 checksum:   347496 9876d8b35bad48cdcb8f1048d2047e9f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-sb1a-bcm91480b_2.6.26-19lenny1_mips.deb
    Size/MD5 checksum:   293606 a1244b2a4a2651b6efb807b0076d0769
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny1_mips.deb
    Size/MD5 checksum:   106228 d5d7bf8693fb6f136b3d7f9fff17868e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-r4k-ip22_2.6.26-19lenny1_mips.deb
    Size/MD5 checksum: 11494216 a913c8d6b075c92143aefab793e49802
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-4kc-malta_2.6.26-19lenny1_mips.deb
    Size/MD5 checksum:   348530 cc0a18212c2ffff502496c88db7353f8
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-r4k-ip22_2.6.26-19lenny1_mips.deb
    Size/MD5 checksum:   252140 303f35d568e062c7c64e1ea77e1acfca
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny1_mips.deb
    Size/MD5 checksum:  3890708 2af87f7f68d961f38631fe9419433cb3

mipsel architecture (MIPS (Little Endian))

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny1_mipsel.deb
    Size/MD5 checksum:  3890674 c698e6416d662e4bdcd5f2b65f519bf5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-sb1-bcm91250a_2.6.26-19lenny1_mipsel.deb
    Size/MD5 checksum:   293840 56463b5f21cba8a85ef4ec09dfb71a2c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-4kc-malta_2.6.26-19lenny1_mipsel.deb
    Size/MD5 checksum: 22903630 2b2d51b9d2c60a0220f19b03f31b0fb1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-sb1-bcm91250a_2.6.26-19lenny1_mipsel.deb
    Size/MD5 checksum: 19553840 04dbd70ded4f1785f2a546c48139e8a4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny1_mipsel.deb
    Size/MD5 checksum:   742188 3bc21117e76e5437436e74c83af2f85c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-mipsel_2.6.26-19lenny1_mipsel.deb
    Size/MD5 checksum:   106278 ec8c866be9034ae2b0235fea0aab8826
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-sb1a-bcm91480b_2.6.26-19lenny1_mipsel.deb
    Size/MD5 checksum: 19544116 385a18ff83f183008d39a324ca3a7a64
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny1_mipsel.deb
    Size/MD5 checksum:   106226 11e40d22e32408ad7e6bc5d5b798341c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-4kc-malta_2.6.26-19lenny1_mipsel.deb
    Size/MD5 checksum:   348886 b9d076bbc0c1c05fee6a8bc701c5246b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-r5k-cobalt_2.6.26-19lenny1_mipsel.deb
    Size/MD5 checksum:   290150 953eede106a61750466539c470fccff3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-sb1a-bcm91480b_2.6.26-19lenny1_mipsel.deb
    Size/MD5 checksum:   293520 9c365550f490f08ed4d1179ea869a2ab
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-5kc-malta_2.6.26-19lenny1_mipsel.deb
    Size/MD5 checksum: 28414750 8958c2bbf25575ad3da01810465c68b0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-r5k-cobalt_2.6.26-19lenny1_mipsel.deb
    Size/MD5 checksum: 14946986 70aa93fb00bac137d0830d5a3d0ae175
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-5kc-malta_2.6.26-19lenny1_mipsel.deb
    Size/MD5 checksum:   347492 3bca2b807dfb73dc9cb723b1acad1c4b

powerpc architecture (PowerPC)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-19lenny1_powerpc.deb
    Size/MD5 checksum:  3810456 0ad5422f7d8e35aa299823d706665589
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny1_powerpc.deb
    Size/MD5 checksum:   743018 bc6ee250e35446a9fa4c966f6d700a4c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-powerpc_2.6.26-19lenny1_powerpc.deb
    Size/MD5 checksum:   360690 934c382889881ed11c3a4d80f46029b5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-powerpc-smp_2.6.26-19lenny1_powerpc.deb
    Size/MD5 checksum:   361350 5ba992ffb657686b695d91142e47c917
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-powerpc_2.6.26-19lenny1_powerpc.deb
    Size/MD5 checksum:   106260 8c9b825ece80ee03756deac85fc7ae85
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-powerpc_2.6.26-19lenny1_powerpc.deb
    Size/MD5 checksum: 23534632 d6cce8ca5ed2f04e67fe6c675989728c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny1_powerpc.deb
    Size/MD5 checksum:  3778166 8ecf326900c7e0a9e0843ff38543e9bf
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-powerpc-smp_2.6.26-19lenny1_powerpc.deb
    Size/MD5 checksum: 23492570 54700bcd87a591d77f04ed3d943f14cc
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-powerpc64_2.6.26-19lenny1_powerpc.deb
    Size/MD5 checksum: 23361112 38fb485837f4e2d2e663276e620b631b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-powerpc_2.6.26-19lenny1_powerpc.deb
    Size/MD5 checksum:   362688 ac80a97076af560d07f2c10ddbcb51b6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny1_powerpc.deb
    Size/MD5 checksum:   106226 008ff2b0a6c9dc7647c4ad764711d8f0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-powerpc64_2.6.26-19lenny1_powerpc.deb
    Size/MD5 checksum:   367612 481b3062c2c8184bea2004519424341f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-powerpc64_2.6.26-19lenny1_powerpc.deb
    Size/MD5 checksum: 23415202 bd5e54388543b1ce52e7b159c7e55f13
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-powerpc_2.6.26-19lenny1_powerpc.deb
    Size/MD5 checksum: 23099378 8b66b3fef4f3824b1153c2ff9f84fe27
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-powerpc64_2.6.26-19lenny1_powerpc.deb
    Size/MD5 checksum:   368056 ae63da437c1c50e250866f9376481f68

s390 architecture (IBM S/390)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-s390-tape_2.6.26-19lenny1_s390.deb
    Size/MD5 checksum:  1628674 2a43d3cc92ec5169f76f00a734b93553
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny1_s390.deb
    Size/MD5 checksum:   106214 4f8c19d44015c514129457f90d4c6977
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-s390_2.6.26-19lenny1_s390.deb
    Size/MD5 checksum:  7486570 2582845c5a0e72efbd370fb510098a71
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-s390x_2.6.26-19lenny1_s390.deb
    Size/MD5 checksum:  7839470 e494833d75eb5d275528ff13810f9a96
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny1_s390.deb
    Size/MD5 checksum:   741408 d75cabed135fc4cf6aa11de352de8c4c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-s390_2.6.26-19lenny1_s390.deb
    Size/MD5 checksum:   106230 c7b39f728c2e34fcf209d2e67354c29b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-s390x_2.6.26-19lenny1_s390.deb
    Size/MD5 checksum:   229672 15bdcc4b04cb7045b0ff2b316e9e7106
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny1_s390.deb
    Size/MD5 checksum:  3530364 0fa6e1ff59144e507927384c8dfb8bd1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-19lenny1_s390.deb
    Size/MD5 checksum:  3562860 17bbd7e48cfade0d92f1613269163b16
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-s390_2.6.26-19lenny1_s390.deb
    Size/MD5 checksum:   228968 70d216d9e8b0c5a93be5104e0b1191ec
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-s390x_2.6.26-19lenny1_s390.deb
    Size/MD5 checksum:   230892 0b4c88be49ec6d67ff26a60e99af0063
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-s390x_2.6.26-19lenny1_s390.deb
    Size/MD5 checksum:  7777622 18ded8a40e6d304b1330d6195973f9f8

sparc architecture (Sun SPARC/UltraSPARC)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-sparc64_2.6.26-19lenny1_sparc.deb
    Size/MD5 checksum:   301122 402b3fee53280fecb1edde00aa5769c4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-19lenny1_sparc.deb
    Size/MD5 checksum:  3813874 1ac8f50ddf17aed04896538831f5e308
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-sparc_2.6.26-19lenny1_sparc.deb
    Size/MD5 checksum:   106216 579c2acaad9bea288586483b75bb4d1c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-sparc64-smp_2.6.26-19lenny1_sparc.deb
    Size/MD5 checksum:   301342 7fc642c64a458417fe84bbc07c81bea3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-sparc64_2.6.26-19lenny1_sparc.deb
    Size/MD5 checksum: 14583642 4a87fbb9193fa19b29b25bd4a6def613
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny1_sparc.deb
    Size/MD5 checksum:   106194 6979522ab7c32fc91b4be1fd9b4aa7be
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-sparc64-smp_2.6.26-19lenny1_sparc.deb
    Size/MD5 checksum: 14564250 e497d698009887bb97cf1b52f4a7184a
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-sparc64_2.6.26-19lenny1_sparc.deb
    Size/MD5 checksum: 14236988 d2c7767f74d10c6c2ae35ca4dba08f8e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-sparc64_2.6.26-19lenny1_sparc.deb
    Size/MD5 checksum:   299712 3693bb4f02e5c8d5d65cba62e69ae847
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny1_sparc.deb
    Size/MD5 checksum:  3778456 089335196a9344fa81df64bee44232fc
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny1_sparc.deb
    Size/MD5 checksum:   797952 9bce00f7843ffaedbe41ca6c9671c398


  These files will probably be moved into the stable distribution on
  its next update.

---------------------------------------------------------------------------------
For apt-get: deb http://security.debian.org/ stable/updates main
For dpkg-ftp: ftp://security.debian.org/debian-security dists/stable/updates/main
Mailing list: debian-security-announce@lists.debian.org
Package info: `apt-cache show <pkg>' and http://packages.debian.org/<pkg>
