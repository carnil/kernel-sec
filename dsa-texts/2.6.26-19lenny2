----------------------------------------------------------------------
Debian Security Advisory DSA-1927-1                security@debian.org
http://www.debian.org/security/                           dann frazier
November 5, 2009                    http://www.debian.org/security/faq
----------------------------------------------------------------------

Package        : linux-2.6
Vulnerability  : privilege escalation/denial of service/sensitive memory leak
Problem type   : local
Debian-specific: no
CVE Id(s)      : CVE-2009-3228 CVE-2009-3238 CVE-2009-3547 CVE-2009-3612
                 CVE-2009-3620 CVE-2009-3621 CVE-2009-3638

Notice: Debian 5.0.4, the next point release of Debian 'lenny', will
include a new default value for the mmap_min_addr tunable.  This
change will add an additional safeguard against a class of security
vulnerabilities known as "NULL pointer dereference" vulnerabilities,
but it will need to be overridden when using certain applications.
Additional information about this change, including instructions for
making this change locally in advance of 5.0.4 (recommended), can be
found at:
  http://wiki.debian.org/mmap_min_addr

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a denial of service, sensitive memory leak or privilege
escalation.  The Common Vulnerabilities and Exposures project
identifies the following problems:

CVE-2009-3228

    Eric Dumazet reported an instance of uninitialized kernel memory
    in the network packet scheduler. Local users may be able to
    exploit this issue to read the contents of sensitive kernel
    memory.
  
CVE-2009-3238

    Linus Torvalds provided a change to the get_random_int() function
    to increase its randomness.

CVE-2009-3547

    Earl Chew discovered a NULL pointer dereference issue in the
    pipe_rdwr_open function which can be used by local users to gain
    elevated privileges.

CVE-2009-3612

    Jiri Pirko discovered a typo in the initialization of a structure
    in the netlink subsystem that may allow local users to gain access
    to sensitive kernel memory.

CVE-2009-3620

    Ben Hutchings discovered an issue in the DRM manager for ATI Rage
    128 graphics adapters. Local users may be able to exploit this
    vulnerability to cause a denial of service (NULL pointer
    dereference).

CVE-2009-3621

    Tomoki Sekiyama discovered a deadlock condition in the UNIX domain
    socket implementation. Local users can exploit this vulnerability
    to cause a denial of service (system hang).

CVE-2009-3638

    David Wagner reported an overflow in the KVM subsystem on i386
    systems. This issue is exploitable by local users with access to
    the /dev/kvm device file.

For the stable distribution (lenny), this problem has been fixed in
version 2.6.26-19lenny2.

For the oldstable distribution (etch), these problems, where
applicable, will be fixed in updates to linux-2.6 and linux-2.6.24.

We recommend that you upgrade your linux-2.6 and user-mode-linux
packages.

Note: Debian carefully tracks all known security issues across every
linux kernel package in all releases under active security support.
However, given the high frequency at which low-severity security
issues are discovered in the kernel and the resource requirements of
doing an update, updates for lower priority issues will normally not
be released for all kernels at the same time. Rather, they will be
released in a staggered or "leap-frog" fashion.

The following matrix lists additional source packages that were
rebuilt for compatibility with or to take advantage of this update:

                                             Debian 5.0 (lenny)
     user-mode-linux                         2.6.26-1um-2+19lenny2

Upgrade instructions
--------------------

wget url
        will fetch the file for you
dpkg -i file.deb
        will install the referenced file.

If you are using the apt-get package manager, use the line for
sources.list as given below:

apt-get update
        will update the internal database
apt-get upgrade
        will install corrected packages

You may use an automated update by adding the resources from the
footer to the proper configuration.

Debian GNU/Linux 5.0 alias lenny
--------------------------------

Stable updates are available for alpha, amd64, armel, hppa, i386,
ia64, and powerpc. Updates for other architectures will be released
as they become available.

Source archives:

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.26-19lenny2.dsc
    Size/MD5 checksum:     5778 8ea6c47c6f227f855a41deea57d988d8
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.26-19lenny2.diff.gz
    Size/MD5 checksum:  7651053 5cf749f9817436c544df97bc0217f125
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.26.orig.tar.gz
    Size/MD5 checksum: 61818969 85e039c2588d5bf3cb781d1c9218bbcb

Architecture independent packages:

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-tree-2.6.26_2.6.26-19lenny2_all.deb
    Size/MD5 checksum:   106866 d25eeb65132ec68406d8fdf7ea340274
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-doc-2.6.26_2.6.26-19lenny2_all.deb
    Size/MD5 checksum:  4627374 196ffe954d4e906638c7eb2bd22e310d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-patch-debian-2.6.26_2.6.26-19lenny2_all.deb
    Size/MD5 checksum:  2565284 0682418bd83f755a17a71435e535f91a
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-source-2.6.26_2.6.26-19lenny2_all.deb
    Size/MD5 checksum: 48672074 5aa4d0110919b100a772509455b22757
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-manual-2.6.26_2.6.26-19lenny2_all.deb
    Size/MD5 checksum:  1768032 cb95ea5101339c35d425ac1ba2f0ff02
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-support-2.6.26-2_2.6.26-19lenny2_all.deb
    Size/MD5 checksum:   122160 0d3dd77a86989aa6e6bdfbbf548d22a6

alpha architecture (DEC Alpha)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-alpha_2.6.26-19lenny2_alpha.deb
    Size/MD5 checksum:   106376 891beea699175e77b6f4cdb1dbbd2377
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-alpha-generic_2.6.26-19lenny2_alpha.deb
    Size/MD5 checksum:   363880 278fefb639e7029af6d5017dedefb500
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-alpha-generic_2.6.26-19lenny2_alpha.deb
    Size/MD5 checksum: 28487296 beb21f0f222b507898406b051d161c25
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny2_alpha.deb
    Size/MD5 checksum:   106358 b4c10db49252b22e7019746743624712
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny2_alpha.deb
    Size/MD5 checksum:   741234 b08b288693ab9d0d3fa1e8141ba4f038
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-alpha-legacy_2.6.26-19lenny2_alpha.deb
    Size/MD5 checksum: 28471478 f412fb78f0dfac51f6e39a035538fe91
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-alpha-smp_2.6.26-19lenny2_alpha.deb
    Size/MD5 checksum:   365312 9147bf190b4dce64fb4783b0c0aba8be
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-alpha-legacy_2.6.26-19lenny2_alpha.deb
    Size/MD5 checksum:   364408 66cd6736f72c0eedabbad596baac8888
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-alpha-smp_2.6.26-19lenny2_alpha.deb
    Size/MD5 checksum: 29177668 abb9bcc21a5fcb0a7352a30fb7209ca1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny2_alpha.deb
    Size/MD5 checksum:  3543732 d84be29426f1d706617a6ad91d3b6109

amd64 architecture (AMD x86_64 (AMD64))

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-amd64_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum:   389134 2ac60b6aaece8351c023cecbb4bd41ee
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum:   749556 c994eeb54dd967b5255448e80fa4911c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-amd64_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum:   389740 8b6b5b10fe023670ca8cf9326d46ccd0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-openvz-amd64_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum:   394262 8398b2d9ce752ffa39ac55b8f55fa1b7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum:  3719144 1fa20cc556fbfecdf0c2335a3c9edeee
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum:   106352 edb758613531f5c655c8451f1136b62a
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-amd64_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum:   106378 dd749481c75a66f517551c6b21b3bbbb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.26-2-xen-amd64_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum: 19274410 21621e01b880d1f222007e3101d255c6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-openvz-amd64_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum: 21053742 015990eedbce234dfa4facdf02f6ad60
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-xen_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum:  3851500 355a9cc7757195196006160929313e78
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-amd64_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum: 20902812 3af1d1431ff5674b7aeaf41c784d3ba6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum:  3751848 f5289bf2c22a6112d13a9af6d4291226
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-xen-amd64_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum:  1804900 8ea5afa2f5e29175e92975ef93144b9a
  http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.26-2-xen-amd64_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum:   106334 2620974dbbc17bbab4aefe183584a6da
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-openvz_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum:  3774804 8fa1254acec879820c17dd8e2e4eee56
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-amd64_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum: 20886016 71a1f29b66ee30cf7a63b77cddc71ec7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-xen-amd64_2.6.26-19lenny2_amd64.deb
    Size/MD5 checksum:   383280 0d0cad637c14a594b3ae424abf824608

armel architecture (ARM EABI)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-iop32x_2.6.26-19lenny2_armel.deb
    Size/MD5 checksum:   365550 f97d5bcae3c5c5957781e6507d730780
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-iop32x_2.6.26-19lenny2_armel.deb
    Size/MD5 checksum: 12396344 04df2ffe832cba3ea1e299701069ca96
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-versatile_2.6.26-19lenny2_armel.deb
    Size/MD5 checksum:   335184 ff1387cae5afb9c7b2d8b20ab546293f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny2_armel.deb
    Size/MD5 checksum:  4136850 e7e7742e3ead70e194f540432bf93ba6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny2_armel.deb
    Size/MD5 checksum:   747792 89242eec0e6f453f37b228ddb49e4e26
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-ixp4xx_2.6.26-19lenny2_armel.deb
    Size/MD5 checksum: 11680082 d9133e003cd603924930f1db870c6d46
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny2_armel.deb
    Size/MD5 checksum:   106354 fce271c39eaa874f6a570b9298a13836
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-versatile_2.6.26-19lenny2_armel.deb
    Size/MD5 checksum:  9575158 d8c6ec6842339c8d8391916c7b4a25c2
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-orion5x_2.6.26-19lenny2_armel.deb
    Size/MD5 checksum: 11371016 edc9b10b99e73302ef1853db546ed6bb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-ixp4xx_2.6.26-19lenny2_armel.deb
    Size/MD5 checksum:   363118 ca61af313ac3687b042c82e4c56bd078
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-armel_2.6.26-19lenny2_armel.deb
    Size/MD5 checksum:   106390 d14317d669c70ea8458b0138105be3e0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-orion5x_2.6.26-19lenny2_armel.deb
    Size/MD5 checksum:   360844 1c7437e1e4de9358f7975feae74501f0

hppa architecture (HP PA RISC)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-parisc64_2.6.26-19lenny2_hppa.deb
    Size/MD5 checksum: 17070158 92d872205303ea622d1419d074b54737
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-parisc_2.6.26-19lenny2_hppa.deb
    Size/MD5 checksum:   296434 df3ddd0a0dbfa712201ff031bfc109c0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-parisc-smp_2.6.26-19lenny2_hppa.deb
    Size/MD5 checksum: 16323830 9998a4deead3033e07f28a1cd0816136
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-parisc64_2.6.26-19lenny2_hppa.deb
    Size/MD5 checksum:   297894 8cace7fc519c562d4b8657c75d230815
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny2_hppa.deb
    Size/MD5 checksum:  3594236 8d621635c43fb9540d4a68ef6d891a57
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny2_hppa.deb
    Size/MD5 checksum:   106356 f967499d62622f5f0833539c9eaf2359
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-hppa_2.6.26-19lenny2_hppa.deb
    Size/MD5 checksum:   106380 f518c1de9ce8dd272db1afa30e38999a
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-parisc_2.6.26-19lenny2_hppa.deb
    Size/MD5 checksum: 15731364 d50829b0556bc7fef6e8c505db959ee2
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny2_hppa.deb
    Size/MD5 checksum:   759840 faab7849f3cef86fbebc037cbd00fd76
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-parisc64-smp_2.6.26-19lenny2_hppa.deb
    Size/MD5 checksum: 17614856 6311929870350217721f7f194b6ff585
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-parisc64-smp_2.6.26-19lenny2_hppa.deb
    Size/MD5 checksum:   299160 57fd97b01842bbe74e37f443e346d695
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-parisc-smp_2.6.26-19lenny2_hppa.deb
    Size/MD5 checksum:   298110 631076db8957d15ab8b0161a60e31734

i386 architecture (Intel ia32)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-486_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:   398182 6f93bf37534bcfb9162b9985b83ee38f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-openvz-686_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum: 20502134 d39255c90c67fddda4c3cb49ce6c93e1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-686_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum: 20235868 99b3ed110df3b6b2bb6b06feb9d30b72
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:   106354 835280ec5ad990b0bcebb988953bd5d9
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-686-bigmem_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum: 20326344 9192cd01f84e7192159aefec2c4f8fb9
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-686_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum: 20208578 c118b5d6fc4f5007728d1ab804624cd8
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-686_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:   398052 88be8c6ce0726c87f3127e1ea8b1a382
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-486_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum: 20175038 ee7bf2ce4d4557f9fdfb53790627ebac
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:  3719206 0d8393bd6245aa3d23ef8938477d5f63
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-686-bigmem_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum: 20353680 67f48fcd0835fd230e8583cf2676cf09
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-686-bigmem_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:   398494 bf4ef1c3e9f35ec4dc0bfaeda1ee5516
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-xen_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:  3851592 94a16944e91f5594a6fa02115b680434
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-686-bigmem_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:   400332 d734fb2f035f0a6a041d13f5a3d95c6c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:   749582 26580da1f40ffeeb17146765bbe241f8
  http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.26-2-xen-686_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:   106348 b76709d63441fcc3e285d2a6dc999890
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-amd64_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum: 20864938 cc5255ece9764242c63b522abfd8a517
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-686_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:   399328 c929aa19b40e7eea5ea885148c645a17
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:  3751908 3b936dbeaf13b730ab8dd56e5ab726f9
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-amd64_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:   387338 03fd54819fb7176a176eeb4c2ff0209c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-openvz-686_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:   403790 efa7179643f2f709cace01bb3f4a5580
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-openvz_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:  3774936 088f38a8e9c79bb4ddc67e200ebee754
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-xen-686_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:  1591850 93ad5d17c9e8ac22c3544c8a9ad9eabd
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-xen-686_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:   384698 5cc9137a10772a48628b0014e0dbbc15
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-i386_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum:   106404 04d07f928e22a2150a2bb9188c6f1257
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.26-2-xen-686_2.6.26-19lenny2_i386.deb
    Size/MD5 checksum: 18035618 641b34424aad0e9291713bd9e2bf96e5

ia64 architecture (Intel ia64)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-itanium_2.6.26-19lenny2_ia64.deb
    Size/MD5 checksum:   355640 2bce0c1faefc019460e3eebca333a5fc
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny2_ia64.deb
    Size/MD5 checksum:  3654768 d8fb31f9660b7c0ab42c77e89bf82f1f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-itanium_2.6.26-19lenny2_ia64.deb
    Size/MD5 checksum:   355064 cfb3eee78e3860b2e650716d5032bf5d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-19lenny2_ia64.deb
    Size/MD5 checksum:  3687386 2980814479dbd08d39bd9f92d3005838
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-mckinley_2.6.26-19lenny2_ia64.deb
    Size/MD5 checksum:   355046 62fc734ea7fe9bc4bef1f8d8b65cc027
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-mckinley_2.6.26-19lenny2_ia64.deb
    Size/MD5 checksum: 34349456 5cfb3ccf034f0ce13a5861507c4cb758
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-itanium_2.6.26-19lenny2_ia64.deb
    Size/MD5 checksum: 34103026 3cee486177d22e2fcd816b536d7ac3d3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny2_ia64.deb
    Size/MD5 checksum:   106350 6265837dd3c0105bcba9d40c5b6966f9
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-mckinley_2.6.26-19lenny2_ia64.deb
    Size/MD5 checksum:   355698 27152c116ad66c7862f3890d36ac80ab
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-mckinley_2.6.26-19lenny2_ia64.deb
    Size/MD5 checksum: 34288678 1540b7be96fbb68e4cc01d858c5ef5a4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-ia64_2.6.26-19lenny2_ia64.deb
    Size/MD5 checksum:   106384 bfb7eeaec3d89587561c56afec1816e9
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-itanium_2.6.26-19lenny2_ia64.deb
    Size/MD5 checksum: 34165098 7a4fbe457d07807a74e9950a47975d49
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny2_ia64.deb
    Size/MD5 checksum:   748220 03f583157c7eef60269042b9a5a6d0bc

powerpc architecture (PowerPC)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-19lenny2_powerpc.deb
    Size/MD5 checksum:   106358 5431bb9d2abe49fc1b186f44bf440cba
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-19lenny2_powerpc.deb
    Size/MD5 checksum:   756032 fb287119a4cf07ef9d6d633ad30f7236
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-powerpc64_2.6.26-19lenny2_powerpc.deb
    Size/MD5 checksum:   372504 9c0501a81bf32b1d0b8c939830d9789b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-powerpc_2.6.26-19lenny2_powerpc.deb
    Size/MD5 checksum: 23650232 ece0b68e6c9baa2e0f964d2bc7da21a2
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-19lenny2_powerpc.deb
    Size/MD5 checksum:  3856256 5a6eb8c2fe7930456cf5f3a1c257fed1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-powerpc64_2.6.26-19lenny2_powerpc.deb
    Size/MD5 checksum: 23514630 0aa445df9e479dc6e266a97658c5c675
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-powerpc64_2.6.26-19lenny2_powerpc.deb
    Size/MD5 checksum: 23453120 7fdf0e57cb3324433e8f5d3e71c5cb7c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-powerpc-smp_2.6.26-19lenny2_powerpc.deb
    Size/MD5 checksum: 23619598 7eb565a76c6ab3318d32c134f7da26b0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-powerpc-smp_2.6.26-19lenny2_powerpc.deb
    Size/MD5 checksum:   366586 3e8f8e0d8d9dc83a3e009bbdcca04d21
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-19lenny2_powerpc.deb
    Size/MD5 checksum:  3890668 a75da89a00e2b5118869888ea03580ae
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-powerpc64_2.6.26-19lenny2_powerpc.deb
    Size/MD5 checksum:   373766 78d152d9edb14f5d179dde50a0131ea7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-powerpc_2.6.26-19lenny2_powerpc.deb
    Size/MD5 checksum:   366686 4b13a456e727a9259685b74132c5b730
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-powerpc_2.6.26-19lenny2_powerpc.deb
    Size/MD5 checksum:   106396 33f493756428189d3acc36bde21631ed
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-powerpc_2.6.26-19lenny2_powerpc.deb
    Size/MD5 checksum:   365950 4149c4f9e6f3e0dc0fbb639a2f962cf8
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-powerpc_2.6.26-19lenny2_powerpc.deb
    Size/MD5 checksum: 23216978 b0034a3be5877f2edebf6ec71c70a83e

  These files will probably be moved into the stable distribution on
  its next update.

---------------------------------------------------------------------------------
For apt-get: deb http://security.debian.org/ stable/updates main
For dpkg-ftp: ftp://security.debian.org/debian-security dists/stable/updates/main
Mailing list: debian-security-announce@lists.debian.org
Package info: `apt-cache show <pkg>' and http://packages.debian.org/<pkg>
