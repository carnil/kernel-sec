----------------------------------------------------------------------
Debian Security Advisory DSA-2928-1                security@debian.org
http://www.debian.org/security/                           Dann Frazier
May 14, 2014                        http://www.debian.org/security/faq
----------------------------------------------------------------------

Package        : linux-2.6
Vulnerability  : privilege escalation/denial of service/information leak
Problem type   : local
Debian-specific: no
CVE Id(s)      : CVE-2014-0196 CVE-2014-1737 CVE-2014-1738

Several vulnerabilities have been discovered in the Linux kernel that may lead
to a denial of service, information leak or privilege escalation. The Common
Vulnerabilities and Exposures project identifies the following problems:

CVE-2014-0196

    Jiri Slaby discovered a race condition in the pty layer, which could lead
    to a denial of service or privilege escalation.

CVE-2014-1737 CVE-2014-1738

    Matthew Daley discovered an information leak and missing input
    sanitising in the FDRAWCMD ioctl of the floppy driver. This could result
    in a privilege escalation.

For the oldstable distribution (squeeze), this problem has been fixed in
version 2.6.32-48squeeze6.

The following matrix lists additional source packages that were rebuilt for
compatibility with or to take advantage of this update:

                                             Debian 6.0 (squeeze)
     user-mode-linux                         2.6.32-1um-4+48squeeze6

We recommend that you upgrade your linux-2.6 and user-mode-linux packages.

Note: Debian carefully tracks all known security issues across every
linux kernel package in all releases under active security support.
However, given the high frequency at which low-severity security
issues are discovered in the kernel and the resource requirements of
doing an update, updates for lower priority issues will normally not
be released for all kernels at the same time. Rather, they will be
released in a staggered or "leap-frog" fashion.

Further information about Debian Security Advisories, how to apply
these updates to your system and frequently asked questions can be
found at: http://www.debian.org/security/

Mailing list: debian-security-announce@lists.debian.org
