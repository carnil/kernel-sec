--------------------------------------------------------------------------
Debian Security Advisory DSA XXX-1                     security@debian.org
http://www.debian.org/security/                               Dann Frazier
XXXXX 8th, 2006                         http://www.debian.org/security/faq
--------------------------------------------------------------------------

Package        : kernel-source-2.6.8
Vulnerability  : several
Problem-Type   : local/remote
Debian-specific: no
CVE ID         : CVE-2006-3468 CVE-2004-2660 CVE-2005-4798 CVE-2006-2935
                 CVE-2006-2936 CVE-2006-1052 CVE-2006-1343 CVE-2006-1528
                 CVE-2006-1855 CVE-2006-1856 CVE-2006-2444 CVE-2006-2446
                 CVE-2006-3745 CVE-2006-4535 CVE-2006-4093 CVE-2006-4145

Several local and remote vulnerabilities have been discovered in the Linux
kernel that may lead to a denial of service or the execution of arbitrary
code. The Common Vulnerabilities and Exposures project identifies the
following problems:

CVE-2006-3468

    James McKenzie discovered a vulnerability in the NFS subsystem, allowing
    remote denial of service if an ext3 filesystem is exported.

CVE-2004-2660

    IWAMOTO Toshihiro discovered a direct IO memory leak that a malicious
    local user could use to create a local denial of service.

CVE-2005-4798

    Assar discovered a buffer overlow in the NFS readlink handling code
    that would allows a malicious remote server to cause a denail of
    service (crash) using a long symlink.

CVE-2006-2935

    Diego Calleja Garcia discovered a potential buffer overflow in the
    dvd_read_bca() function that could allow aribrary code execution via
    a malicious CDROM device

CVE-2006-2936

    Ian Abbott and Guillaume Autran provided a fix for a vulnerability in
    the ftdio_sio driver that could allow a local user to initiate a denial
    of service attack by writing lots of data to the serial port and
    consuming all of system memory.

CVE-2006-1052

    Stephen Smalley contributed a fix for a bug in SELinux that allows local
    users with ptrace permission to change the tracer SID to the SID of
    another process.

CVE-2006-1343

    Pavel Kankovsky discovered that sockaddr_in.sin_zero is not zeroed
    during certain operations returning IPv4 socket names which allows
    potentially sensitive memory to be leaked to userspace.

CVE-2006-1528

    Douglas Gilbert reported a bug in the sg driver that allows local
    users to oops the kernel by performing dio transfers from the sg
    driver to memory mapped IO space.

CVE-2006-1855

    Mattia Belletti noticed that certain debugging code left in the
    choose_new_parent routine allows local users to cause a denial of
    service (panic).

CVE-2006-1856

    Kostik Belousov discovered a missing LSM file_permission check in the
    readv and writev functions which might allow attackers to bypass intended
    access restrictions.

CVE-2006-2444

    Patrick McHardy reported a memory corruption bug in snmp_trap_decode that
    could be used by remote attackers to crash a system.

CVE-2006-2446

    A race between the kfree_skb and __skb_unlink functions allows remote
    users to crash a system.

CVE-2006-3745

    Wei Wang discovered a vulnerability in the SCTP subsystem that can be
    exploited for local privilege escalation.


CVE-2006-4535

    David Miller reported a problem with the fix for CVE-2006-3745 that allows
    local users to crash the system using via an SCTP socket with a certain
    SO_LINGER value.

CVE-2006-4093

    Olof Johansson reported a vulnerability on PPC970 systems that allows
    local users to hang a machine related to the HID0 attention enable at
    boot time.

CVE-2006-4145

    Colin discovered a bug in the UDF filesystem that allows local users to
    hang a system when truncating files.

The following matrix explains which kernel version for which architecture
fix the problems mentioned above:

                                 Debian 3.1 (sarge)
     Source                      2.6.8-16sarge5
     Alpha architecture          2.6.8-16sarge5
     AMD64 architecture          2.6.8-16sarge5
     HP Precision architecture   2.6.8-6sarge5
     Intel IA-32 architecture    2.6.8-16sarge5
     Intel IA-64 architecture    2.6.8-14sarge5
     Motorola 680x0 architecture 2.6.8-4sarge5
     PowerPC architecture        2.6.8-12sarge5
     IBM S/390 architecture      2.6.8-5sarge5
     Sun Sparc architecture      2.6.8-15sarge5

The following matrix lists additional packages that were rebuilt for
compatibility with or to take advantage of this update:

                                 Debian 3.1 (sarge)
     fai-kernels                 1.9.1sarge4

We recommend that you upgrade your kernel package immediately and reboot
the machine. If you have built a custom kernel from the kernel source
package, you will need to rebuild to take advantage of these fixes.

Upgrade Instructions
--------------------

wget url
        will fetch the file for you
dpkg -i file.deb
        will install the referenced file.

If you are using the apt-get package manager, use the line for
sources.list as given below:

apt-get update
        will update the internal database
apt-get upgrade
        will install corrected packages

You may use an automated update by adding the resources from the
footer to the proper configuration.


Debian GNU/Linux 3.1 alias sarge
--------------------------------


  These files will probably be moved into the stable distribution on
  its next update.

---------------------------------------------------------------------------------
For apt-get: deb http://security.debian.org/ stable/updates main
For dpkg-ftp: ftp://security.debian.org/debian-security dists/stable/updates/main
Mailing list: debian-security-announce@lists.debian.org
Package info: `apt-cache show <pkg>' and http://packages.debian.org/<pkg>
