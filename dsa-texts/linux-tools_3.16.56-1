From:  <>
To: debian-security-announce@lists.debian.org
Subject: [SECURITY] [DSA 4179-1] linux-tools security update

-------------------------------------------------------------------------
Debian Security Advisory DSA-4179-1                   security@debian.org
https://www.debian.org/security/                                         
April 22, 2018                        https://www.debian.org/security/faq
-------------------------------------------------------------------------

Package        : linux-tools

This update doesn't fix a vulnerability in linux-tools, but provides
support for building Linux kernel modules with the "retpoline"
mitigation for CVE-2017-5715 (Spectre variant 2).

This update also includes bug fixes from the upstream Linux 3.16 stable
branch up to and including 3.16.56.

For the oldstable distribution (jessie), this problem has been fixed
in version 3.16.56-1.

We recommend that you upgrade your linux-tools packages.

For the detailed security status of linux-tools please refer to
its security tracker page at:
https://security-tracker.debian.org/tracker/linux-tools

Further information about Debian Security Advisories, how to apply
these updates to your system and frequently asked questions can be
found at: https://www.debian.org/security/

Mailing list: debian-security-announce@lists.debian.org
